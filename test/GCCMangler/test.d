import cpz.GCCMangler;
import cpz.common;
import std.stdio;

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"foo1"};
    enum string mangled = f.mangle!(int function())();
    static assert(mangled == "_ZN3Foo4foo1Ev");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"foo1"};
    enum string mangled = f.mangle!(int function(ubyte))();
    static assert(mangled == "_ZN3Foo4foo1Eh");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"foo2"};
    enum string mangled = f.mangle!(int function(byte))();
    static assert(mangled == "_ZN3Foo4foo2Ea");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"foo3"};
    enum string mangled = f.mangle!(int function(ushort))();
    static assert(mangled == "_ZN3Foo4foo3Et");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"foo4"};
    enum string mangled = f.mangle!(int function(short))();
    static assert(mangled == "_ZN3Foo4foo4Es");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"foo5"};
    enum string mangled = f.mangle!(int function(uint))();
    static assert(mangled == "_ZN3Foo4foo5Ej");
    writeln(mangled);
}
unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"foo6"};
    enum string mangled = f.mangle!(int function(int))();
    static assert(mangled == "_ZN3Foo4foo6Ei");
    writeln(mangled);
}
unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"foo7"};
    enum string mangled = f.mangle!(int function(ulong))();
    static assert(mangled == "_ZN3Foo4foo7Em");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"foo8"};
    enum string mangled = f.mangle!(int function(long))();
    static assert(mangled == "_ZN3Foo4foo8El");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"bar1"};
    enum string mangled = f.mangle!(int function(float))();
    static assert(mangled == "_ZN3Foo4bar1Ef");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"bar2"};
    enum string mangled = f.mangle!(int function(double))();
    static assert(mangled == "_ZN3Foo4bar2Ed");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"baz1"};
    enum string mangled = f.mangle!(int function(int, float))();
    static assert(mangled == "_ZN3Foo4baz1Eif");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"baz2"};
    enum string mangled = f.mangle!(int function(int*))();
    static assert(mangled == "_ZN3Foo4baz2EPi");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"baz3"};
    enum string mangled = f.mangle!(int function(char*))();
    static assert(mangled == "_ZN3Foo4baz3EPc");
    writeln(mangled);
}

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"baz4"};
    enum string mangled = f.mangle!(int function(char))();
    static assert(mangled == "_ZN3Foo4baz4Ec");
    writeln(mangled);
}

//extern(C) int _ZN3Foo4baz5ES_(Foo* _this, Foo notthis);
//extern(C) int _ZN3Foo4baz6EPS_(Foo* _this, Foo* notthis);
//extern(C) int _ZN3Foo4baz7E6Warbar(Foo* _this, Warbar notthis);
//extern(C) int _ZN3Foo4baz8EP6Warbar(Foo* _this, Warbar* notthis);

unittest {
    enum _CppFunction f = {classname:"Foo", funcname:"baz9"};
    enum string mangled = f.mangle!(int function(...))();
    static assert(mangled == "_ZN3Foo4baz9Ez");
    writeln(mangled);
}

void main() {
}

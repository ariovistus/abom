#include "stuff.h"
#include <iostream>

using namespace std;
void assert(bool test, char *msg) {
    if (!test) {
        cout << "ERROR: " << msg << "\n";
    }
}

int main(int argc, char **argv) {
    cout << "base1: " << sizeof(Base1) << "\n";
    cout << "base2: " << sizeof(Base2) << "\n";
    cout << "derived: " << sizeof(Derived) << "\n";
    Base1 *base1 = SomeBase1();
    cout << "base1 " << ((size_t)(&base1->data_Base1) - (size_t)base1) << "\n";
    assert(base1->data_Base1 == 123, "");
    base1->speakClearly();
    assert(base1->data_Base1 == 26, "");
    Base1 *copy1 = base1->clone();
    assert(copy1->data_Base1 == 26, "");

    Base2 *base2 = SomeBase2();
    cout << "base2 " << ((size_t)(&base2->data_Base2) - (size_t)base2) << "\n";
    assert(base2->data_Base2 == 222, "");
    base2->mumble();
    assert(base2->data_Base2 == 312, "");
    Base2 *copy2 = base2->clone();
    assert(copy2->data_Base2 == 312, "");

    Derived *derv = SomeDerived();
    assert(derv->data_Base1 == 123, "");
    assert(derv->data_Base2 == 222, "");
    assert(derv->data_Derived == 441, "");
    cout << "base1 offset: " << (((size_t)&derv->data_Base1)-((size_t)derv)) << "\n";
    cout << "base2 offset: " << (((size_t)&derv->data_Base2)-((size_t)derv)) << "\n";
    cout << "derv offset: " << (((size_t)&derv->data_Derived)-((size_t)derv)) << "\n";
    derv->speakClearly();
    assert(derv->data_Base1 == 26, "");
    derv->mumble();
    assert(derv->data_Base2 == 312, "");
    Derived *copyd = derv->clone();
    assert(copyd->data_Base1 == 26, "");
    assert(copyd->data_Base2 == 312, "");
    assert(copyd->data_Derived == 441, "");
}

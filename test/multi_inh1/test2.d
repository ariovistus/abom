import std.stdio;

struct Base1 {
    static struct _Base1_vtable_ {
        extern(C) void function(Base1* _this) dtor1;
        extern(C) void function(Base1* _this) dtor2;
        extern(C) void function(Base1* _this) speakClearly;
        extern(C) Base1* function(Base1* _this) clone;
    }
    _Base1_vtable_* _vtable_;
    int data_Base1;
}
struct Base2 {
    static struct _Y_vtable_ {
        extern(C) void function(Base2* _this) dtor1;
        extern(C) void function(Base2* _this) dtor2;
        extern(C) void function(Base2* _this) mumble;
        extern(C) Base2* function(Base2* _this) clone;
    }
    _Y_vtable_* _vtable_;
    int data_Base2;
}

struct Derived {
    static struct _Derived_vtable_ {
        extern(C) void function(Derived* _this) dtor1;
        extern(C) void function(Derived* _this) dtor2;
        extern(C) void function(Base1* _this) speakClearly;
        extern(C) Derived* function(Derived* _this) clone;
        size_t somejunk;
        size_t someotherjunk;
        extern(C) void function(Derived* _this) dtor_thunk1;
        extern(C) void function(Derived* _this) dtor_thunk2;
        extern(C) void function(Base2* _this) mumble;
        extern(C) Base2* function(Derived* _this) clone_thunk;
    }
    struct _hacky1 {
        Base1 _base1;
        Base2 _base2;
    }
    struct _hacky2 {
        Base1 _base1;
        typeof(Base2.tupleof) tuple;
        int data_Derived;
    }
    union _hacky {
        _hacky1 h1;
        _hacky2 h2;
    }
    _hacky h;
}

T static_cast(T)(Derived* xd) {
    static if(is(T == Base1*)) {
        return &xd.h.h1._base1;
    }else static if(is(T == Base2*)) {
        return &xd.h.h1._base2;
    }
}

extern(C) void _ZN5Base112speakClearlyEv(Base1* _this);
extern(C) Base1* _ZNK5Base15cloneEv(Base1* _this);
extern(C) void _ZN5Base26mumbleEv(Base2* _this);
extern(C) Base2* _ZNK5Base25cloneEv(Base2* _this);
extern(C) Derived* _ZNK7Derived5cloneEv(Derived* _this);

extern(C) Base1* _Z9SomeBase1v(); 
extern(C) Base2* _Z9SomeBase2v(); 
extern(C) Derived* _Z11SomeDerivedv();

void main() {
    writeln("base1: ", Base1.sizeof);
    writeln("base2: ", Base2.sizeof);
    writeln("derived: ", Derived.sizeof);
    Base1* base1 = _Z9SomeBase1v();
    assert(base1._vtable_.speakClearly == &_ZN5Base112speakClearlyEv);
    assert(base1._vtable_.clone == &_ZNK5Base15cloneEv);
    assert(base1.data_Base1 == 123, "");
    base1._vtable_.speakClearly(base1);
    assert(base1.data_Base1 == 26, "");
    Base1* copy1 = base1._vtable_.clone(base1);
    assert(copy1.data_Base1 == 26, "");

    Base2* base2 = _Z9SomeBase2v();
    assert(base2._vtable_.mumble == &_ZN5Base26mumbleEv);
    assert(base2._vtable_.clone == &_ZNK5Base25cloneEv);
    assert(base2.data_Base2 == 222, "");
    base2._vtable_.mumble(base2);
    assert(base2.data_Base2 == 312, "");
    Base2* copy2 = base2._vtable_.clone(base2);
    assert(copy2.data_Base2 == 312, "");

    Derived* derv = _Z11SomeDerivedv();
    auto derv_base1 = static_cast!(Base1*)(derv);
    auto derv_base2 = static_cast!(Base2*)(derv);
    auto vtable = (cast(Derived._Derived_vtable_*) (derv_base1._vtable_));
    assert(vtable.speakClearly == &_ZN5Base112speakClearlyEv);
    assert(vtable.clone == &_ZNK7Derived5cloneEv);
    assert(derv_base1.data_Base1 == 123, "");
    assert(derv_base2.data_Base2 == 222, "");
    writefln("base1 offset: %d", cast(size_t)( cast(void*)&derv.h.h1._base1.data_Base1 - cast(void*)derv));
    assert(8 == cast(size_t)( cast(void*)&derv.h.h1._base1.data_Base1 - cast(void*)derv));
    writefln("base2 offset: %d", cast(size_t)( cast(void*)&derv.h.h1._base2.data_Base2 - cast(void*)derv));
    writefln("derv offset: %d", cast(size_t)( cast(void*)&derv.h.h2.data_Derived - cast(void*)derv));
    assert(28 == cast(size_t)( cast(void*)&derv.h.h2.data_Derived - cast(void*)derv));
    assert(derv.h.h2.data_Derived == 441, "");
    vtable.speakClearly(static_cast!(Base1*)(derv));
    assert(derv_base1.data_Base1 == 26, "");
    vtable.mumble(static_cast!(Base2*)(derv));
    assert(derv_base2.data_Base2 == 312, "");
    Derived* copyd = vtable.clone(derv);
    auto copyd_base1 = static_cast!(Base1*)(derv);
    auto copyd_base2 = static_cast!(Base2*)(derv);
    vtable = (cast(Derived._Derived_vtable_*) (copyd_base1._vtable_));
    assert(copyd_base1.data_Base1 == 26, "");
    assert(copyd_base2.data_Base2 == 312, "");
    assert(copyd.h.h2.data_Derived == 441, "");
}

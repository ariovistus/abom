#include <stdlib.h>

class Base1 {
    public:
        Base1();
        virtual ~Base1();
        virtual void speakClearly();
        virtual Base1 *clone() const;
        int data_Base1;
};

class Base2 {
    public:
        Base2();
        virtual ~Base2();
        virtual void mumble();
        virtual Base2 *clone() const;
        int data_Base2;
};

class Derived : public Base1, public Base2 {
    public:
        Derived();
        virtual ~Derived();
        virtual Derived *clone() const;
        int data_Derived;
};

Base1 *SomeBase1();
Base2 *SomeBase2();
Derived *SomeDerived();

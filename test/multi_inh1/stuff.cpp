#include "stuff.h"
#include <cstdio>

Base1::Base1() : data_Base1(123) {
}

Base1::~Base1() {
}

void Base1::speakClearly(){
    puts("I like tacos");
    this->data_Base1 = 26;
}

Base1 *Base1::clone() const {
    Base1 *newobj = new Base1();
    newobj->data_Base1 = this->data_Base1;
    return newobj;
}

Base2::Base2() : data_Base2(222) {
}

Base2::~Base2() {
}

void Base2::mumble() {
    puts("mrm mrm tacos");
    this->data_Base2 = 312;
}

Base2 *Base2::clone() const {
    Base2 *newobj = new Base2();
    newobj->data_Base2 = this->data_Base2;
    return newobj;
}

Derived::Derived() : data_Derived(441) {
}

Derived::~Derived() {
}

Derived *Derived::clone() const {
    Derived *newobj = new Derived();
    newobj->data_Base1 = this->data_Base1;
    newobj->data_Base2 = this->data_Base2;
    newobj->data_Derived = this->data_Derived;
    return newobj;
}


Base1 *SomeBase1() {
    return new Base1(); 
}
Base2 *SomeBase2() {
    return new Base2(); 
}
Derived *SomeDerived() {
    return new Derived(); 
}

import std.stdio;
import std.c.stdlib;

struct X {
    int i;
}

extern(C) int _ZN1X1yEv(X* _this);
extern(C) X* _Z5SomeXv(); 
extern(C) void _ZN1XC1Ev(X* _this);
extern(C) void _ZN1X1zEi(X* _this, int i);
extern(C) int _ZN1X3abuEi(int i);

void main() {
    assert(X.sizeof == 4);
    X *x = _Z5SomeXv();
    _ZN1XC1Ev(x);
    int i = _ZN1X1yEv(x);
    writeln("i: ", i);
    assert(i == 1);
    _ZN1X1zEi(x,2);
    i = _ZN1X1yEv(x);
    writeln("i: ", i);
    assert(i == 2);
    i = _ZN1X3abuEi(i);
    assert(i == 3);
}

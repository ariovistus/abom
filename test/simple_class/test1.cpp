#include "stuff.h"
#include <iostream>

using namespace std;
int main() {
    X *x = SomeX();
    x->z(2);
    cout << x->y() << "\n";
    cout << "X.i: " << (&((X*)0)->i) << "\n";
    cout << "sizeof X: " << sizeof(X) << "\n";
    if (sizeof(X) != 4) {
        cout << "ERROR: sizeof X should be 4\n";
    }
}

//import std.stdio;
import abom.GCC;
import std.stdio;

mixin CppClass!("X", 
    typeof(new class {
        int i;
        int y();
        void z(int j);

        static int abu(int j);
    }));

mixin(CppFunction!("SomeX", X* function()));

void main() {
    //assert(X.sizeof == 4);
    X* x = SomeX();
    int i = x.y();
    assert(i == 1);
    x.z(2);
    i = x.y();
    writeln(i);
    assert(i == 2);
    i = X.abu(i);
    assert(i == 3);
}

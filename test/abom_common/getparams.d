import abom.common;
void foo(int i, int j=2) {
}
void foo2(ref int i) {
}

static assert(getparams!(foo,"P","Pd") == "P[0] i, P[1] j = Pd[1]");
static assert(getparams!(foo2,"P","Pd") == "ref P[0] i");
// sad day - dmd doesn't store this info (?)
static assert(getparams!(int function(int i, int j=2),"P","Pd") == 
    "P[0] , P[1] ");
static assert(getparams!(int function(ref int i, int j=2),"P","Pd") == 
    "ref P[0] , P[1] ");

void main() {}

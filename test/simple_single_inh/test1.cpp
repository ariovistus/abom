#include "stuff.h"
#include <cstddef>
#include <iostream>

using namespace std;
void assert(bool test, char *msg) {
    if (!test) {
        cout << "ERROR: " << msg << "\n";
    }
}
int main() {
    X *x = SomeX();
    x->z(12);
    assert(x->y() == 12, "y should be 12");
    x->z();
    assert(x->y() == 7, "y should be 7");
    cout << "sizeof X: " << sizeof(X) << "\n";
    cout << "X.i: " << offsetof(X,i) << "\n";
    cout << "sizeof XDerived: " << sizeof(XDerived) << "\n";
    cout << "XDerived.i: " << offsetof(XDerived, i) << "\n";
    cout << "XDerived.j: " << offsetof(XDerived, j) << "\n";
    x = SomeXDerived();
    x->z(2);
    assert(x->y() == 53, "y should be 53");
    assert(sizeof(X) == 16, "sizeof X should be 16");
    assert(sizeof(XDerived) == 24, "sizeof XDerived should be 24");
}

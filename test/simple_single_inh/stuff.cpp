#include "stuff.h"

X::X() {
    this->i = 1;
}
int X::y(){
    return this->i;
}
int XDerived::y(){
    return this->i + 51;
}
void X::z(int j){
    this->i = j;
}

XDerived::XDerived() : j(44.0) {
    this->i = 3;
}

int XDerived::aa(int t) {
    return this->i * 12 + ((int) (this->j + 7)) % t;
}

X *SomeX() {
    return new X(); 
}
XDerived *SomeXDerived() {
    return new XDerived(); 
}

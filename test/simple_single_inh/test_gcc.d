import abom.GCC;
import abom.common;

mixin CppClass!("X", 
    typeof(new class {
        int i;
        @CppVirtual
        int y();
        @CppVirtual
        void z(int j = 7);
    }));
mixin CppClass!("XDerived", 
        CppInherits!X,
    typeof(new class {
        int j;
        int aa(int t);
        @CppVirtual
        void z(int j = 7);
    }));

mixin(CppFunction!("SomeX", X* function()));
mixin(CppFunction!("SomeXDerived", XDerived* function()));

void main() {
}

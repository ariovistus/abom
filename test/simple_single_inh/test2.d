import std.stdio;
import std.c.stdlib;

struct X {
    static struct _X_vtable_ {
        extern(C) int function(X* _this) y;
        extern(C) void function(X* _this, int i = 7) z;
    }
    _X_vtable_* _vtable_;
    int i;
}

struct XDerived {
    X._X_vtable_* _x_vtable_;
    int i;
    double j;
}

extern(C) int _ZN1X1yEv(X* _this);
extern(C) int _ZN8XDerived1yEv(XDerived* _this);
extern(C) X* _Z5SomeXv(); 
extern(C) XDerived* _Z12SomeXDerivedv(); 
extern(C) void _ZN1XC1Ev(X* _this);
extern(C) void _ZN1X1zEi(X* _this, int i = 7);

void main() {
    X *x = _Z5SomeXv();
    assert(x._vtable_.y == &_ZN1X1yEv);
    assert(x._vtable_.z == &_ZN1X1zEi);
    int i = _ZN1X1yEv(x);
    assert(i == 1);
    _ZN1X1zEi(x,2);
    i = _ZN1X1yEv(x);
    assert(i == 2);
    i = x._vtable_.y(x);
    assert(i == 2);
    x._vtable_.z(x, 12);
    i = _ZN1X1yEv(x);
    assert(i == 12);

    x = cast(X*) _Z12SomeXDerivedv();
    assert(x._vtable_.y == cast(typeof(&_ZN1X1yEv))&_ZN8XDerived1yEv);
    assert(x._vtable_.z == &_ZN1X1zEi);
    i = _ZN1X1yEv(x);
    assert(i == 3);
    i = x._vtable_.y(x);
    assert(i == 54);

    XDerived* x2 = _Z12SomeXDerivedv();
    assert(x2._x_vtable_.y == cast(typeof(&_ZN1X1yEv))&_ZN8XDerived1yEv);
    assert(x2._x_vtable_.z == &_ZN1X1zEi);
    i = _ZN8XDerived1yEv(x2);
    assert(i == 54);
    i = x2._x_vtable_.y(cast(X*) x2);
    assert(i == 54);

    assert(X.sizeof == 16);
    assert(XDerived.sizeof == 24);
    //writeln("XDerived i: ", XDerived.i.offsetof);
    //writeln("XDerived j: ", XDerived.j.offsetof);
    assert(XDerived.i.offsetof == 8);
    assert(XDerived.j.offsetof == 16);
}

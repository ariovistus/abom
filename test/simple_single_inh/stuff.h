class X {
    public:
        int i;

        X();
        virtual int y();
        virtual void z(int j = 7);
};

class XDerived : public X {
    public:
        double j;

        XDerived();
        int aa(int t);
        virtual int y();
};

X *SomeX();
XDerived *SomeXDerived();

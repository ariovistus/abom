import abom.GCC;
import abom.common;

mixin CppClass!("Foo", 
    CppTemplateParams("X"),
    typeof(new class {
        int goo(TArg!"X" *r);
    }));

mixin(CppFunction!("SomeFoo", Foo!int* function()));
mixin(CppFunction!("DumbFoo", void function(Foo!int*)));
//extern(C) int _ZN3FooIiE3gooEPi(Foo!int*, int*);
//extern(C) Foo!int* _Z7SomeFoov();
//extern(C) void _Z7DumbFooP3FooIiE(Foo!int*);

void main() {
    Foo!int* foo = SomeFoo();
    int j = 2;
    int i = foo.goo(&j);
    assert(i == 6);
    DumbFoo(foo);
}

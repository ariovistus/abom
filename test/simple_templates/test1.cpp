#include "stuff.h"
#include <iostream>

using namespace std;
int main() {
    int j;
    Foo<int> *x = SomeFoo();
    cout << x->goo(&j) << "\n";
    cout << "sizeof Foo: " << sizeof(Foo<int>) << "\n";
    if (sizeof(Foo<int>) != 1) {
        cout << "ERROR: sizeof X should be 1\n";
    }
}

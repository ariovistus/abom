#include <vector>
template<class X>
class Foo{
    public:
        int goo(X *r) {
            return 2 + sizeof(X);
        }

};

Foo<int> *SomeFoo() {
    return new Foo<int>();
}

void DumbFoo(Foo<int> *thing) {
    int i = 1;
    thing->goo(&i);
}


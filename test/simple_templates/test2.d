
struct Foo(T) {

}
extern(C) int _ZN3FooIiE3gooEPi(Foo!int*, int*);
extern(C) Foo!int* _Z7SomeFoov();
extern(C) void _Z7DumbFooP3FooIiE(Foo!int*);

void main() {
    Foo!int* foo = _Z7SomeFoov();
    int j = 2;
    int i = _ZN3FooIiE3gooEPi(foo, &j);
    assert(i == 6);
    _Z7DumbFooP3FooIiE(foo);
}

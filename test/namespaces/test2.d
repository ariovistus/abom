import std.stdio;
import std.c.stdlib;

struct ChocolateFactory {
}
extern(C) ChocolateFactory* _ZN5Willy5Wonka20SomeChocolateFactoryEv();
extern(C) int _ZN5Willy5Wonka16ChocolateFactory13MakeChocolateEv(ChocolateFactory* _this);
extern(C) int _ZN5Willy5Wonka16ChocolateFactory13ScareChildrenEv(ChocolateFactory* _this);
extern(C) int _ZN5Willy5Wonka16ChocolateFactory17AbuseUumpaLuumpasEv(ChocolateFactory* _this);
extern(C) int _ZN5Willy5Wonka16ChocolateFactory12MakeChildrenEv(ChocolateFactory* _this);
extern(C) int _ZN5Willy5Wonka16ChocolateFactory26ScareUumpaLuumpasChocolateEv(ChocolateFactory* _this);
extern(C) int _ZN5Willy5Wonka16ChocolateFactory33AbuseChocolateUumpaLuumpaChildrenEv(ChocolateFactory* _this);

void main() {
    assert(ChocolateFactory.sizeof == 1);
    ChocolateFactory *x = _ZN5Willy5Wonka20SomeChocolateFactoryEv();
    int i = _ZN5Willy5Wonka16ChocolateFactory13MakeChocolateEv(x);
    writeln("i: ", i);
    assert(i == 1);
    i = _ZN5Willy5Wonka16ChocolateFactory13ScareChildrenEv(x);
    writeln("i: ", i);
    assert(i == 2);
    i = _ZN5Willy5Wonka16ChocolateFactory17AbuseUumpaLuumpasEv(x);
    writeln("i: ", i);
    assert(i == 3);
    i = _ZN5Willy5Wonka16ChocolateFactory12MakeChildrenEv(x);
    writeln("i: ", i);
    assert(i == 4);
    i = _ZN5Willy5Wonka16ChocolateFactory26ScareUumpaLuumpasChocolateEv(x);
    writeln("i: ", i);
    assert(i == 5);
    i = _ZN5Willy5Wonka16ChocolateFactory33AbuseChocolateUumpaLuumpaChildrenEv(x);
    writeln("i: ", i);
    assert(i == 6);
}

#include "stuff.h"
#include <iostream>

using namespace std;
using namespace Willy::Wonka;
int main() {
    ChocolateFactory *x = SomeChocolateFactory();
    x->MakeChocolate();
    cout << x->ScareChildren() << "\n";
    cout << "sizeof ChocolateFactory: " << sizeof(ChocolateFactory) << "\n";
    if (sizeof(ChocolateFactory) != 1) {
        cout << "ERROR: sizeof ChocolateFactory should be 1\n";
    }
}

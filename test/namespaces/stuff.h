namespace Willy {
    namespace Wonka {
        class ChocolateFactory {
            public:
                int MakeChocolate();
                int ScareChildren();
                int AbuseUumpaLuumpas();
                int MakeChildren();
                int ScareUumpaLuumpasChocolate();
                int AbuseChocolateUumpaLuumpaChildren();
        };

        ChocolateFactory *SomeChocolateFactory() {
            return new ChocolateFactory();
        }
    }
}

import std.stdio;
import std.c.stdlib;
import abom.GCC;
import abom.common;

mixin CppClass!("ChocolateFactory", 
        CppNamespace("Willy::Wonka"),
        typeof(new class {
            int MakeChocolate();
            int ScareChildren();
            int AbuseUumpaLuumpas();
            int MakeChildren();
            int ScareUumpaLuumpasChocolate();
            int AbuseChocolateUumpaLuumpaChildren();
        }));

mixin(CppFunction!("SomeChocolateFactory", ChocolateFactory* function(), CppNamespace("Willy::Wonka")));

void main() {
    assert(ChocolateFactory.sizeof == 1);
    ChocolateFactory *x = SomeChocolateFactory();
    int i = x.MakeChocolate();
    writeln("i: ", i);
    assert(i == 1);
    i = x.ScareChildren();
    writeln("i: ", i);
    assert(i == 2);
    i = x.AbuseUumpaLuumpas();
    writeln("i: ", i);
    assert(i == 3);
    i = x.MakeChildren();
    writeln("i: ", i);
    assert(i == 4);
    i = x.ScareUumpaLuumpasChocolate();
    writeln("i: ", i);
    assert(i == 5);
    i = x.AbuseChocolateUumpaLuumpaChildren();
    writeln("i: ", i);
    assert(i == 6);
}

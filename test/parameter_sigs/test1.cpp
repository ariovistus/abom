#include "stuff.h"
#include <iostream>

using namespace std;
int main() {
    Foo *x = SomeFoo();
    cout << "sizeof X: " << sizeof(Foo) << "\n";
    if (sizeof(Foo) != 1) {
        cout << "ERROR: sizeof X should be 1\n";
    }
}

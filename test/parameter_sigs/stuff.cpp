#include "stuff.h"
int Foo::foo1(uint8_t i) { return 1 + i;}
int Foo::foo2(int8_t i) { return 2 + i;}
int Foo::foo3(uint16_t i) { return 3 + i;}
int Foo::foo4(int16_t i) { return 4 + i;}
int Foo::foo5(uint32_t i) { return 5 + i;}
int Foo::foo6(int32_t i) { return 6 + i;}
int Foo::foo7(uint64_t i) { return 7 + (int)i;}
int Foo::foo8(int64_t i) { return 8 + (int)i;}
int Foo::foo9(bool i) { return 9 + (int)i;}
double Foo::bar1(float i) { return 100.5 + i;}
double Foo::bar2(double i) { return 200.0 + i;}

int Foo::baz1(int i, float t) { return 99 + i; }
int Foo::baz2(int *i) { return 77; }
int Foo::baz3(char *i) { return 33; }
int Foo::baz4(char i) { return 35 + i; }
int Foo::baz5(Foo i) { return 131;}
int Foo::baz6(Foo *i) { return 141;}
int Foo::baz7(Warbar i) { return 132;}
int Foo::baz8(Warbar *i) { return 142;}
int Foo::baz9(...) { return 154;}
int Foo::baz10(int &i) { 
    i = 2;
    return 155;
}
int Foo::baz11(const int &i) { return 156; }
int Foo::baz12(const int *i) { return 157; }
int Foo::baz13(const int i) { return 158; }

import std.stdio;
import std.c.stdlib;
import abom.GCC;
import abom.common;
import core.vararg;

@ACppClass
@CppNamespace("")
struct Warbar {
}
/*
struct Foo {
}
*/

mixin CppClass!("Foo", typeof(new class {
    int foo1(ubyte i);
    int foo2(byte i);
    int foo3(ushort i);
    int foo4(short i);
    int foo5(uint i);
    int foo6(int i);
    int foo7(ulong i);
    int foo8(long i);
    int foo9(bool i);
    double bar1(float i);
    double bar2(double i);

    int baz1(int i, float t);
    int baz2(int* i);
    int baz3(char* i);
    int baz4(char i);
    int baz5(ThisType i);
    int baz6(ThisType* i);
    int baz7(Warbar i);
    int baz8(Warbar* i);
    int baz9(...);
    int baz10(ref int i);
    int baz11(const ref int i);
    int baz12(const int* i);
    int baz13(const int i);
}));

mixin(CppFunction!("SomeFoo", Foo* function()));

void main() {
    writeln("sizeof Foo: ", Foo.sizeof);
    assert(Foo.sizeof == 1);
    Foo *x = SomeFoo(); 
    int i = x.foo1(cast(ubyte) 22);
    writeln("i: ", i);
    assert(i == 23);
    i = x.foo2(cast(ubyte) 22);
    writeln("i: ", i);
    assert(i == 24);
    i = x.foo3(cast(ushort) 22);
    writeln("i: ", i);
    assert(i == 25);
    i = x.foo4(cast(short) 22);
    writeln("i: ", i);
    assert(i == 26);
    i = x.foo5(cast(uint) 22);
    writeln("i: ", i);
    assert(i == 27);
    i = x.foo6(cast(int) 22);
    writeln("i: ", i);
    assert(i == 28);
    i = x.foo7(cast(ulong) 22);
    writeln("i: ", i);
    assert(i == 29);
    i = x.foo8(cast(long) 22);
    writeln("i: ", i);
    assert(i == 30);
    i = x.foo9(cast(bool) 1);
    writeln("i: ", i);
    assert(i == 10);
    double d = x.bar1(22f);
    writeln("d: ", d);
    assert(d == 122.5);
    d = x.bar2(22f);
    writeln("d: ", d);
    assert(d == 222.0);
    i = x.baz1(22, 0.0);
    writeln("i: ", i);
    assert(i == 121);
    i = x.baz2(&i);
    writeln("i: ", i);
    assert(i == 77);
    i = x.baz3(cast(char*)&i);
    writeln("i: ", i);
    assert(i == 33);
    i = x.baz4('a');
    writeln("i: ", i);
    assert(i == 35 + 'a');
    i = x.baz5(*x);
    writeln("i: ", i);
    assert(i == 131);
    Warbar warbar;
    i = x.baz7(warbar);
    writeln("i: ", i);
    assert(i == 132);
    i = x.baz8(&warbar);
    writeln("i: ", i);
    assert(i == 142);
    i = x.baz9(&warbar);
    writeln("i: ", i);
    assert(i == 154);
    int j = 1;
    i = x.baz10(j);
    writeln("i: ", i);
    assert(i == 155);
    assert(j == 2);
    j = 1;
    i = x.baz11(j);
    writeln("i: ", i);
    assert(i == 156);
    assert(j == 1);
    i = x.baz12(&j);
    writeln("i: ", i);
    assert(i == 157);
    assert(j == 1);
    i = x.baz13(j);
    writeln("i: ", i);
    assert(i == 158);
}

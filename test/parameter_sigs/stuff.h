#include <stdint.h>

class Warbar {
};
class Foo {
    int foo1(uint8_t i);
    int foo2(int8_t i);
    int foo3(uint16_t i);
    int foo4(int16_t i);
    int foo5(uint32_t i);
    int foo6(int32_t i);
    int foo7(uint64_t i);
    int foo8(int64_t i);
    int foo9(bool i);
    double bar1(float i);
    double bar2(double i);

    int baz1(int i, float t);
    int baz2(int *i);
    int baz3(char *i);
    int baz4(char i);
    int baz5(Foo i);
    int baz6(Foo *i);
    int baz7(Warbar i);
    int baz8(Warbar *i);
    int baz9(...);
    int baz10(int &i);
    int baz11(const int &i);
    int baz12(const int *i);
    int baz13(const int i);
};

Foo *SomeFoo() {
    return new Foo();
}

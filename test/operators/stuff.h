
class Foo {
    public:
        int operator+(int i);
        int operator+=(int i);
        int operator*(int i);
        int operator*=(int i);
        int operator-(int i);
        int operator-=(int i);
        int operator/(int i);
        int operator/=(int i);
        int operator%(int i);
        int operator%=(int i);
        int operator=(int i);
        int operator()(int i);
        int operator[](int i);
        int operator+();
        int operator~();
        int operator-();
        int operator*();
        int operator->();
        int operator++();
        int operator--();
        int operator==(Foo i);
        int operator<(int i);
        int operator>(int i);
        int operator<=(int i);
        int operator>=(int i);
};

Foo *SomeFoo();

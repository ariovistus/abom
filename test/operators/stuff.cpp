#include "stuff.h"
int Foo::operator+(int i) {
    return i + 121;
}
int Foo::operator+=(int i) {
    return i + 122;
}
int Foo::operator*(int i) {
    return i + 131;
}
int Foo::operator*=(int i) {
    return i + 132;
}
int Foo::operator-(int i) {
    return i + 144;
}
int Foo::operator-=(int i) {
    return i + 145;
}
int Foo::operator/(int i) {
    return 153+i;
}
int Foo::operator/=(int i) {
    return 154+i;
}
int Foo::operator%(int i) {
    return 155+i;
}
int Foo::operator%=(int i) {
    return 156+i;
}
int Foo::operator=(int i) {
    return i + 141;
}
int Foo::operator()(int i) {
    return i + 151;
}
int Foo::operator[](int i) {
    return i + 150;
}
int Foo::operator+() {
    return 102;
}
int Foo::operator~() {
    return 103;
}
int Foo::operator-() {
    return 104;
}
int Foo::operator*() {
    return 105;
}
int Foo::operator->() {
    return 201;
}
int Foo::operator++() {
    return 202;
}
int Foo::operator--() {
    return 203;
}
int Foo::operator==(Foo i) {
    return 210;
}
int Foo::operator<(int i) {
    return i+211;
}
int Foo::operator>(int i) {
    return i+212;
}
int Foo::operator<=(int i) {
    return i+213;
}
int Foo::operator>=(int i) {
    return i+214;
}

Foo *SomeFoo() {
    return new Foo();
}

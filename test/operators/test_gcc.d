import std.stdio;
import abom.GCC;
import abom.common;

mixin CppClass!("Foo", 
    typeof(new class {
        @CppOperator("+")
        int plus(int i); 
        @CppOperator("+=")
        int pluseq(int i); 
        @CppOperator("-")
        int minus(int i);
        @CppOperator("-=")
        int minuseq(int i);
        @CppOperator("*")
        int mul(int i);
        @CppOperator("*=")
        int muleq(int i);
        @CppOperator("/")
        int div(int i);
        @CppOperator("/=")
        int diveq(int i);
        @CppOperator("%")
        int mod(int i);
        @CppOperator("%=")
        int modeq(int i);
        @CppOperator("=")
        int asg(int i);
        @CppOperator("()")
        int call(int i);
        @CppOperator("[]")
        int index(int i);
        @CppOperator(">")
        int gt(int i);
        @CppOperator(">=")
        int ge(int i);
        @CppOperator("<")
        int lt(int i);
        @CppOperator("<=")
        int le(int i);
        @CppOperator("+")
        int unaryPlus();
        @CppOperator("~")
        int tilde();
        @CppOperator("-")
        int neg();
        @CppOperator("*")
        int deref();
        @CppOperator("->")
        int pt();
        @CppOperator("++")
        int plusplus();
        @CppOperator("--")
        int minusminus();
        @CppOperator("==")
        int eq(ThisType other);


    }));
mixin(Foo.Externs);

mixin(CppFunction!("SomeFoo", Foo* function()));

void main() {
    Foo* foo = SomeFoo();
    int i = foo.plus(1);
    writeln("i: ", i);
    assert(i == 122);
    i = foo.pluseq(1);
    writeln("i: ", i);
    assert(i == 123);
    i = foo.mul(1);
    writeln("i: ", i);
    assert(i == 132);
    i = foo.muleq(1);
    writeln("i: ", i);
    assert(i == 133);
    i = foo.minus(1);
    writeln("i: ", i);
    assert(i == 145);
    i = foo.minuseq(1);
    writeln("i: ", i);
    assert(i == 146);
    i = foo.div(1);
    writeln("i: ", i);
    assert(i == 154);
    i = foo.diveq(1);
    writeln("i: ", i);
    assert(i == 155);
    i = foo.mod(1);
    writeln("i: ", i);
    assert(i == 156);
    i = foo.modeq(1);
    writeln("i: ", i);
    assert(i == 157);
    i = foo.asg(1);
    writeln("i: ", i);
    assert(i == 142);
    i = foo.call(1);
    writeln("i: ", i);
    assert(i == 152);
    i = foo.index(1);
    writeln("i: ", i);
    assert(i == 151);
    i = foo.unaryPlus();
    writeln("i: ", i);
    assert(i == 102);
    i = foo.tilde();
    writeln("i: ", i);
    assert(i == 103);
    i = foo.neg();
    writeln("i: ", i);
    assert(i == 104);
    i = foo.deref();
    writeln("i: ", i);
    assert(i == 105);
    i = foo.pt();
    writeln("i: ", i);
    assert(i == 201);
    i = foo.plusplus();
    writeln("i: ", i);
    assert(i == 202);
    i = foo.minusminus();
    writeln("i: ", i);
    assert(i == 203);
    i = foo.eq(*foo);
    writeln("i: ", i);
    assert(i == 210);
    i = foo.gt(1);
    writeln("i: ", i);
    assert(i == 213);
    i = foo.lt(1);
    writeln("i: ", i);
    assert(i == 212);
    i = foo.ge(1);
    writeln("i: ", i);
    assert(i == 215);
    i = foo.le(1);
    writeln("i: ", i);
    assert(i == 214);
}

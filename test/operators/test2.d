import std.stdio;
struct Foo {
}

extern(C) Foo* _Z7SomeFoov();
extern(C) int _ZN3FooplEi(Foo* _this, int i); // +
extern(C) int _ZN3FoopLEi(Foo* _this, int i); // +=
extern(C) int _ZN3FoomiEi(Foo* _this, int i); // -
extern(C) int _ZN3FoomIEi(Foo* _this, int i); // -=
extern(C) int _ZN3FoomlEi(Foo* _this, int i); // *
extern(C) int _ZN3FoomLEi(Foo* _this, int i); // *=
extern(C) int _ZN3FoodvEi(Foo* _this, int i); // /
extern(C) int _ZN3FoodVEi(Foo* _this, int i); // /=
extern(C) int _ZN3FoormEi(Foo* _this, int i); // %
extern(C) int _ZN3FoorMEi(Foo* _this, int i); // %=
extern(C) int _ZN3FooaSEi(Foo* _this, int i); // =
extern(C) int _ZN3FooclEi(Foo* _this, int i); // ( )
extern(C) int _ZN3FooixEi(Foo* _this, int i); // [ ] 
extern(C) int _ZN3FoogtEi(Foo* _this, int i); // >
extern(C) int _ZN3FooltEi(Foo* _this, int i); // <
extern(C) int _ZN3FoogeEi(Foo* _this, int i); // >=
extern(C) int _ZN3FooleEi(Foo* _this, int i); // <=
extern(C) int _ZN3FoopsEv(Foo* _this); // +
extern(C) int _ZN3FoocoEv(Foo* _this); // ~
extern(C) int _ZN3FoongEv(Foo* _this); // -
extern(C) int _ZN3FoodeEv(Foo* _this); // *
extern(C) int _ZN3FooptEv(Foo* _this); // ->
extern(C) int _ZN3FooppEv(Foo* _this); // ++
extern(C) int _ZN3FoommEv(Foo* _this); // --
extern(C) int _ZN3FooeqES_(Foo* _this, Foo notthis); // ==


void main() {
    Foo* foo = _Z7SomeFoov();
    int i = _ZN3FooplEi(foo,1);
    writeln("i: ", i);
    assert(i == 122);
    i = _ZN3FoopLEi(foo,1);
    writeln("i: ", i);
    assert(i == 123);
    i = _ZN3FoomlEi(foo,1);
    writeln("i: ", i);
    assert(i == 132);
    i = _ZN3FoomLEi(foo,1);
    writeln("i: ", i);
    assert(i == 133);
    i = _ZN3FoomiEi(foo,1);
    writeln("i: ", i);
    assert(i == 145);
    i = _ZN3FoomIEi(foo,1);
    writeln("i: ", i);
    assert(i == 146);
    i = _ZN3FoodvEi(foo,1);
    writeln("i: ", i);
    assert(i == 154);
    i = _ZN3FoodVEi(foo,1);
    writeln("i: ", i);
    assert(i == 155);
    i = _ZN3FoormEi(foo,1);
    writeln("i: ", i);
    assert(i == 156);
    i = _ZN3FoorMEi(foo,1);
    writeln("i: ", i);
    assert(i == 157);
    i = _ZN3FooaSEi(foo,1);
    writeln("i: ", i);
    assert(i == 142);
    i = _ZN3FooclEi(foo,1);
    writeln("i: ", i);
    assert(i == 152);
    i = _ZN3FooixEi(foo,1);
    writeln("i: ", i);
    assert(i == 151);
    i = _ZN3FoopsEv(foo);
    writeln("i: ", i);
    assert(i == 102);
    i = _ZN3FoocoEv(foo);
    writeln("i: ", i);
    assert(i == 103);
    i = _ZN3FoongEv(foo);
    writeln("i: ", i);
    assert(i == 104);
    i = _ZN3FoodeEv(foo);
    writeln("i: ", i);
    assert(i == 105);
    i = _ZN3FooptEv(foo);
    writeln("i: ", i);
    assert(i == 201);
    i = _ZN3FooppEv(foo);
    writeln("i: ", i);
    assert(i == 202);
    i = _ZN3FoommEv(foo);
    writeln("i: ", i);
    assert(i == 203);
    i = _ZN3FooeqES_(foo, *foo);
    writeln("i: ", i);
    assert(i == 210);
    i = _ZN3FoogtEi(foo,1);
    writeln("i: ", i);
    assert(i == 213);
    i = _ZN3FooltEi(foo,1);
    writeln("i: ", i);
    assert(i == 212);
    i = _ZN3FoogeEi(foo,1);
    writeln("i: ", i);
    assert(i == 215);
    i = _ZN3FooleEi(foo,1);
    writeln("i: ", i);
    assert(i == 214);
}

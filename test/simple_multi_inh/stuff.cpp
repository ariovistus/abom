#include "stuff.h"

X::X() {
    this->i = 1;
}
int X::y(){
    return this->i;
}
int XDerived::y(){
    return this->i + 41;
}
void X::z(int j){
    this->i = j;
}

Y::Y() : g("abc"), len(3) {
}

char *Y::gamma() {
    return this->g;
}

size_t Y::lemma() {
    return this->len;
}

size_t XDerived::lemma() {
    return this->len + 100;
}


XDerived::XDerived() : j(44.0) {
    this->i = 3;
}

int XDerived::aa(int t) {
    return this->i * 12 + ((int) (this->j + 7)) % t;
}

Z::Z() : vedic(0x1212121) { }

unsigned int Z::getVedic() {
    return this->vedic;
}
void Z::setVedic(unsigned int v) {
    this->vedic = v;
}

XDerived2::XDerived2() : slappy(1.1), trappy(2.2) {
}
size_t XDerived2::lemma() {
    return this->len + 200;
}
int XDerived2::tater(int t) {
    return (int) (this->slappy + this->trappy);
}
int XDerived2::y() {
    return this->i - 11;
}

void XDerived2::setVedic(unsigned int v) {
    this->vedic = v + 11;
}

X *SomeX() {
    return new X(); 
}
Y *SomeY() {
    return new Y(); 
}
Z *SomeZ() {
    return new Z();
}
XDerived *SomeXDerived() {
    return new XDerived(); 
}
XDerived2 *SomeXDerived2() {
    return new XDerived2(); 
}

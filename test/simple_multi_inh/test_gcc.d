import abom.GCC;
import abom.common;
import std.conv;

mixin CppClass!("X",
    typeof(new class {
        int i;
        @CppVirtual
        int y();
        @CppVirtual
        void z(int j = 7);
    }));

mixin CppClass!("Y",
    typeof(new class {
        char* g;
        size_t len;

        @CppVirtual 
        char* gamma();
        @CppVirtual 
        size_t lemma();
    }));

mixin CppClass!("Z",
    typeof(new class {
        uint vedic;
        @CppVirtual
        uint getVedic();
        @CppVirtual
        void setVedic(uint v);

    }));

mixin CppClass!("XDerived",
    CppInherits!(X, Y),
    typeof(new class {
        double j;

        @CppVirtual size_t lemma();

        int aa(int t);
        @CppVirtual
        int y();
    }));
mixin(CppFunction!("SomeX", X* function()));
mixin(CppFunction!("SomeY", Y* function()));
mixin(CppFunction!("SomeZ", Z* function()));
mixin(CppFunction!("SomeXDerived", XDerived* function()));

extern(C) int _ZN1X1yEv(X* _this);
extern(C) void _ZN1X1zEi(X* _this, int i);
extern(C) char* _ZN1Y5gammaEv(Y* _this);
extern(C) size_t _ZN1Y5lemmaEv(Y* _this);
extern(C) int _ZN8XDerived1yEv(XDerived* _this);
extern(C) void _ZN1X1zEi(X* _this, int i);
extern(C) size_t _ZThn16_N8XDerived5lemmaEv(Y* _this);
extern(C) size_t _ZN8XDerived5lemmaEv(XDerived* _this);

void main(){
    //assert(X.sizeof == 16);
    //assert(XDerived.sizeof == 48);
    X* x = SomeX();
    assert(x._vtable.y == &_ZN1X1yEv);
    assert(x._vtable.z == &_ZN1X1zEi);
    int i = x.y();
    assert(i == 1);
    x.z(12);
    i = x.y();
    assert(i == 12, "y should be 12");

    Y* y = SomeY();
    assert(y._vtable.lemma == &_ZN1Y5lemmaEv);
    assert(y._vtable.gamma == &_ZN1Y5gammaEv);
    char* gg = "abc".dup.ptr;
    //y.g = gg;
    //assert(_ZN1Y5gammaEv(y) == y.g);
    //assert(_ZN1Y5lemmaEv(y) == 3);

    XDerived* xd = SomeXDerived();
    assert(xd._vtable.y == &_ZN8XDerived1yEv);
    assert(xd._vtable.z == cast(typeof(xd._vtable.z))&_ZN1X1zEi);
    assert(xd._vtable.lemma == cast(typeof(xd._vtable.lemma))&_ZN8XDerived5lemmaEv);
    // stinking thunks

    assert(xd._vtable.gamma == cast(typeof(xd._vtable.gamma)) &_ZN1Y5gammaEv);
    assert(xd._vtable.lemma_thunk1 == cast(typeof(xd._vtable.lemma_thunk1))&_ZThn16_N8XDerived5lemmaEv);
    i = xd.y();
    import std.stdio;
    writeln("i:", i);
    assert(i == 44);
    // todo: should there be a way to call X::z with an instance of XDerived?
    xd.z(12);
    i = xd.y();
    assert(i == 53, "y should be 53");

    assert((cast(void*) &xd.Super!(Y*).g - cast(void*)xd) == 0x18);
    char* g2 = xd.gamma();
    assert(to!string(g2) == "abc"); 
}

#include "stuff.h"
#include <cstdio>
#include <iostream>

using namespace std;
void assert(bool test, char *msg) {
    if (!test) {
        cout << "ERROR: " << msg << "\n";
    }
}
int main() {
    X *x = SomeX();
    x->z(12);
    assert(x->y() == 12, "y should be 12");
    x->z();
    assert(x->y() == 7, "y should be 7");

    Y *y = SomeY();
    assert(y->lemma() == 3, "lemma should be 3");

    XDerived *xd = SomeXDerived();
    xd->z(12);
    assert(xd->y() == 53, "y should be 53");
    assert(xd->lemma() == 103, "lemma should be 103");

    y = static_cast<Y*>(xd);

    size_t xdv = reinterpret_cast<size_t>(xd);
    size_t yv = reinterpret_cast<size_t>(y);

    XDerived2 *xd2 = SomeXDerived2();
    xd2->z(12);
    assert(xd2->y() == 1, "y should be 1");
    assert(xd2->lemma() == 203, "lemma should be 203");

    assert(yv == xdv + sizeof(X), "XDerived wasn't cast to Y the way I thought it was");
    cout << x->y() << "\n";
    cout << "sizeof X: " << sizeof(X) << "\n";
    cout << "X.i: " << (&((X*)0)->i) << "\n";
    cout << "sizeof XDerived: " << sizeof(XDerived) << "\n";
    cout << "XDerived.i: " << (&((XDerived*)0)->i) << "\n";
    cout << "XDerived.j: " << (&((XDerived*)0)->j) << "\n";
    cout << "XDerived.g: " << (&((XDerived*)0)->g) << "\n";
    cout << "XDerived.len: " << (&((XDerived*)0)->len) << "\n";
    assert(sizeof(X) == 16, "sizeof X should be 16");
    assert(sizeof(XDerived) == 48, "sizeof XDerived should be 48");
}

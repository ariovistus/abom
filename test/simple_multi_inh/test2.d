import std.stdio;
import std.c.stdlib;

struct X {
    static struct _X_vtable_ {
        extern(C) int function(X* _this) y;
        extern(C) void function(X* _this, int j = 7) z;
    }
    _X_vtable_* _vtable_;
    int i;
}
struct Y {
    static struct _Y_vtable_ {
        extern(C) char* function(Y* _this) gamma;
        extern(C) size_t function(Y* _this) lemma;
    }
    _Y_vtable_* _vtable_;
    char* g;
    size_t len;
}

struct Z {
    static struct _Z_vtable_ {
        extern(C) uint function(Z* _this) getVedic;
        extern(C) void function(Z* _this, uint v) setVedic;
    }
    _Z_vtable_* _vtable_;
    uint vedic;
}

struct XDerived {
    static struct _XDerived_vtable_ {
        extern(C) int function(XDerived* _this) y;
        extern(C) void function(X* _this, int j = 7) z;
        extern(C) size_t function(XDerived* _this) lemma;
        size_t somejunk;
        size_t someotherjunk;
        extern(C) char* function(Y* _this) gamma;
        extern(C) size_t function(XDerived* _this) lemma_thunk;
    }
    //_XDerived_vtable_* _vtable_;
    X _x;
    Y _y;
    double j;
}

struct XDerived2 {
    static struct _XDerived2_vtable_ {
        extern(C) int function(XDerived2* _this) y;
        extern(C) void function(X* _this, int j = 7) z;
        extern(C) size_t function(XDerived2* _this) lemma;
        extern(C) int function(XDerived2* _this, int t) tater;
        extern(C) uint function(XDerived2* _this, uint v) setVedic;
        size_t somejunk;
        size_t someotherjunk;
        extern(C) char* function(Y* _this) gamma;
        extern(C) size_t function(Y* _this) lemma_thunk;
        size_t somejunk2;
        size_t someotherjunk2;
        extern(C) uint function(Z* _this) getVedic;
        extern(C) void function(Z* _this, uint v) setVedic_thunk;
    }
    //_XDerived_vtable_* _vtable_;
    X _x;
    Y _y;
    Z _z;
    double slappy;
    float trappy;
}

T static_cast(T, Zt)(Zt* xd) if(is(Zt == XDerived) || is(Zt == XDerived2)) {
    static if(is(T == X*)) {
        return &xd._x;
    }else static if(is(T == Y*)) {
        return &xd._y;
    }else static if(is(T == Z*)) {
        return &xd._z;
    }
}

extern(C) int _ZN1X1yEv(X* _this);
extern(C) int _ZN8XDerived1yEv(XDerived* _this);
extern(C) int _ZN9XDerived21yEv(XDerived2* _this);
extern(C) void _ZN1X1zEi(X* _this, int i);
extern(C) char* _ZN1Y5gammaEv(Y* _this);
extern(C) size_t _ZN1Y5lemmaEv(Y* _this);
extern(C) size_t _ZN8XDerived5lemmaEv(XDerived* _this);
extern(C) X* _Z5SomeXv(); 
extern(C) Y* _Z5SomeYv(); 
extern(C) Z* _Z5SomeZv();
extern(C) XDerived* _Z12SomeXDerivedv();
extern(C) XDerived2* _Z13SomeXDerived2v();
extern(C) void _ZN1XC1Ev(X* _this);
extern(C) size_t _ZThn16_N8XDerived5lemmaEv(Y* _this);
extern(C) uint _ZN1Z8getVedicEv(Z* _this);
extern(C) void _ZN1Z8setVedicEj(Z* _this, uint v);
extern(C) int _ZN9XDerived25taterEi(XDerived2* _this, int t);
extern(C) size_t _ZN9XDerived25lemmaEv(XDerived2* _this);
extern(C) void _ZN9XDerived28setVedicEj(XDerived2* _this, uint v);

void main() {
    writeln("XDerived.i: ", &((cast(XDerived*)null)._x.i));
    writeln("XDerived.j: ", &((cast(XDerived*)null).j)); 
    writeln("XDerived.g: ", &((cast(XDerived*)null)._y.g));
    writeln("XDerived.len: ", &((cast(XDerived*)0)._y.len));
    assert(X.sizeof == 16);
    assert(XDerived.sizeof == 48);
    X* x = _Z5SomeXv();
    assert(x._vtable_.y == &_ZN1X1yEv);
    assert(x._vtable_.z == &_ZN1X1zEi);
    int i = _ZN1X1yEv(x);
    assert(i == 1);
    _ZN1X1zEi(x, 12);
    i = _ZN1X1yEv(x);
    assert(i == 12, "y should be 12");
    x._vtable_.z(x, 13);
    i = x._vtable_.y(x);
    assert(i == 13, "y should be 13");

    Y* y = _Z5SomeYv();
    assert(y._vtable_.lemma == &_ZN1Y5lemmaEv);
    assert(y._vtable_.gamma == &_ZN1Y5gammaEv);
    char* gg = "abc".dup.ptr;
    y.g = gg;
    assert(_ZN1Y5gammaEv(y) == y.g);
    assert(_ZN1Y5lemmaEv(y) == 3);

    XDerived* xd = _Z12SomeXDerivedv();
    writefln("xd x vtable: %x", xd._x._vtable_);
    writefln("xd y vtable: %x", xd._y._vtable_);
    assert(xd._x._vtable_.y == cast(typeof(xd._x._vtable_.y))&_ZN8XDerived1yEv);
    assert(xd._x._vtable_.z == cast(typeof(xd._x._vtable_.z))&_ZN1X1zEi);
    // stinking thunks
    assert(xd._y._vtable_.lemma == cast(typeof(xd._y._vtable_.lemma))&_ZThn16_N8XDerived5lemmaEv);
    assert(xd._y._vtable_.gamma == cast(typeof(xd._y._vtable_.gamma)) &_ZN1Y5gammaEv);
    i = _ZN1X1yEv(static_cast!(X*)(xd));
    writeln(i);
    assert(i == 3);
    _ZN1X1zEi(static_cast!(X*)(xd), 12);
    i = _ZN1X1yEv(static_cast!(X*)(xd));
    assert(i == 12, "y should be 12");
    xd._x._vtable_.z(static_cast!(X*)(xd), 12);
    i = xd._x._vtable_.y(static_cast!(X*)(xd));
    writeln(i);
    assert(i == 53, "y should be 53");

    xd._y.g = gg;
    char* g2 = _ZN1Y5gammaEv(static_cast!(Y*)(xd));
    //writeln("Y offset: ", XDerived._vtable_.offsetof);
    writefln("%x", g2);
    writefln("%x", xd._y.g);
    assert(g2 == xd._y.g); 

    Z* z = _Z5SomeZv();
    assert(z._vtable_.getVedic == &_ZN1Z8getVedicEv);
    assert(z._vtable_.setVedic == &_ZN1Z8setVedicEj);
    _ZN1Z8setVedicEj(z, 0xdeafbeef);
    uint ui = _ZN1Z8getVedicEv(z);
    assert(ui == 0xdeafbeef);
    z._vtable_.setVedic(z, 0xdaffbeef);
    ui = z._vtable_.getVedic(z);
    assert(ui == 0xdaffbeef);

    XDerived2* xd2 = _Z13SomeXDerived2v();
    auto vtable = cast(XDerived2._XDerived2_vtable_*) xd2._x._vtable_;
    assert(vtable.y == cast(typeof(vtable.y))&_ZN9XDerived21yEv);
    assert(vtable.z == cast(typeof(vtable.z))&_ZN1X1zEi);
    assert(vtable.lemma == &_ZN9XDerived25lemmaEv);
    assert(vtable.tater == &_ZN9XDerived25taterEi);
    assert(vtable.setVedic == cast(typeof(vtable.setVedic)) &_ZN9XDerived28setVedicEj);
    assert(cast(void*) &vtable.y == cast(void*) &xd2._x._vtable_.y);
    assert(&vtable.lemma_thunk == &xd2._y._vtable_.lemma);
    assert(&vtable.setVedic_thunk == &xd2._z._vtable_.setVedic);
    assert(xd2._y._vtable_.gamma == &_ZN1Y5gammaEv);

    _ZN1Z8setVedicEj(static_cast!(Z*)(xd2), 0xdeafbeef);
    ui = _ZN1Z8getVedicEv(static_cast!(Z*)(xd2));
    assert(ui == 0xdeafbeef);
    vtable.setVedic(xd2, 0xdaffbeef);
    ui = vtable.getVedic(static_cast!(Z*)(xd2));
    assert(ui == 0xdaffbeef+11);

    assert(xd._y._vtable_.gamma(static_cast!(Y*)(xd)) == xd._y.g);
    assert(xd._y._vtable_.lemma(static_cast!(Y*)(xd)) == 103);

}

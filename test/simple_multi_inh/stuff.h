#include <stdlib.h>

class X {
    public:
        int i;

        X();
        virtual int y();
        virtual void z(int j = 7);
};

class Y {
    public:
        char *g;
        size_t len;

        Y();

        virtual char *gamma();
        virtual size_t lemma();
};

class Z {
    public:
        unsigned int vedic;

        Z();

        virtual unsigned int getVedic();
        virtual void setVedic(unsigned int v);
};

class XDerived : public X, public Y {
    public:
        virtual size_t lemma();
        double j;

        XDerived();
        int aa(int t);
        virtual int y();
};
class XDerived2 : public X, public Y, public Z {
    public:
        double slappy;
        float trappy;
        virtual size_t lemma();

        XDerived2();
        virtual int tater(int t);
        virtual int y();
        virtual void setVedic(unsigned int v);
};

class Zoop {
};
class Poop {
    virtual int two();
};

class Gloop : public Poop, public Zoop {
};

X *SomeX();
Y *SomeY();
Z *SomeZ();
XDerived *SomeXDerived();
XDerived2 *SomeXDerived2();

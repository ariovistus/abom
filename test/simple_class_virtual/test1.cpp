#include "stuff.h"
#include <cstdio>
#include <cstddef>
#include <iostream>

extern "C" {
void print_stuff(size_t *x);
void (*_ZN1X1yEv());
void (*_ZN1X1zEi());
}

struct XV {
    int (*y)(X* _this);
    size_t z;
};

using namespace std;
int main() {
    X *x = SomeX();
    XV *xv = ((XV **) x)[0];
    printf("virtual table [%d]: %x\n", offsetof(XV, y), (void*)xv->y);
    printf("virtual table [%d]: %x\n", offsetof(XV, z), xv->z);
    puts("");
    x->y();
    puts("did one");
    xv->y(x);
    puts("did two");
    printf("virtual table [%d]: %x\n", offsetof(XV, y), (void*)xv->y);
    printf("virtual table [%d]: %x\n", offsetof(XV, z), xv->z);
    x->z(2);
    cout << x->y() << "\n";
    cout << "X.i: " << (&((X*)0)->i) << "\n";
    cout << "&x: " << (size_t)x << "\n";
    cout << "x[0]: " << ((size_t*)x)[0] << "\n";
    cout << "sizeof X: " << sizeof(X) << "\n";
    if (sizeof(X) != 16) {
        cout << "ERROR: sizeof X should be 16\n";
    }
}

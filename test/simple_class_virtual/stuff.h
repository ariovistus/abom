#include <cstdio>
class X {
    public:
        int i;

        X();
        virtual int y();
        virtual void z(int j);
};

X *SomeX();

extern "C" {
void print_stuff(size_t *x) {
    int i = 0;
    unsigned char *bs = (unsigned char*) x;
    for(i = 0; i < 5; i++) {
        printf("%x ", bs[i]);
    }
    printf("\n");
}
}

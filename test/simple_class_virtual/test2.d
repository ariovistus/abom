import std.stdio;
import std.string;
import std.c.stdlib;

struct X {
    static struct _X_vtable_ {
        extern(C) int function(X* _this) y;
        extern(C) void function(X* _this, int j) z;
    }
    _X_vtable_* _vtable_;
    int i;
}

extern(C) int _ZN1X1yEv(X* _this);
extern(C) X* _Z5SomeXv(); 
extern(C) void _ZN1XC1Ev(X* _this);
extern(C) void _ZN1X1zEi(X* _this, int i);
extern(C) void print_stuff(size_t *x);

void main() {
    X* x = _Z5SomeXv();
    assert(x._vtable_.y == &_ZN1X1yEv);
    assert(x._vtable_.z == &_ZN1X1zEi);
    int i = _ZN1X1yEv(x);
    assert(i == 1);
    _ZN1X1zEi(x,2);
    i = _ZN1X1yEv(x);
    assert(i == 2);
    size_t* xs = cast(size_t*) x;
    i = x._vtable_.y(x);
    assert(i == 2);
    x._vtable_.z(x,3);
    i = x._vtable_.y(x);
    assert(i == 3);

    assert(X.sizeof == 16, 
            format("sizeof X should be %s, is %s", 16, X.sizeof));
}

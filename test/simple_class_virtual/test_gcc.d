import abom.GCC;
import abom.common;

mixin CppClass!("X", 
    typeof(new class {
        int i;
        @CppVirtual
        int y();
        @CppVirtual
        void z(int j);
    }));

mixin(CppFunction!("SomeX", X* function()));

extern(C) int _ZN1X1yEv(X* _this);
extern(C) void _ZN1X1zEi(X* _this, int i);

void main() {
    X* x = SomeX();
    // todo: generate these tests
    assert(x._vtable.y == &_ZN1X1yEv);
    assert(x._vtable.z == &_ZN1X1zEi);
    int i = x.y();
    assert(i == 1);
    x.z(2);
    i = x.y();
    assert(i == 2);
    /* todo
    assert(X.sizeof == 16, 
            format("sizeof X should be %s, is %s", 16, X.sizeof));
    */

}

module abom.GCCMangler;

import abom.common;
import std.traits;
import std.string;
import std.conv;

public import abom.params;

string mangle(_Fn...)(_CppFunction cf) if (_Fn.length == 1) {
    alias _Fn[0] Fn;
    static assert(isFunctionPointer!Fn || is(typeof(Fn) == function) || is(Fn == function));

    string [] Namespace = cf.ns_parts();
    string mangled = "_Z";
    if(cf.classname != null || cf.namespace != null) {
        mangled ~= "N";
    }
    foreach(i, ns_part; Namespace) {
        mangled ~= mangle_name(ns_part);
    }
    if(cf.classname.length != 0) {
        mangled ~= mangle_name(cf.classname); 
    }
    if(cf.isoperator) {
        mangled ~= mangle_operator(cf.operator, ParameterTypeTuple!(Fn).length == 0);
    }else{
        mangled ~= mangle_name(cf.funcname); 
    }
    if(cf.classname != null || cf.namespace != null) {
        mangled ~= "E";
    }
    mangled ~= mangle_sigs!Fn(cf);
    return mangled;
}

string mangle_name(string name, bool enclose = false) {
    string mangled;
    mangled ~= to!string(name.length);
    mangled ~= name;
    return mangled;
}

string mangle_operator(string op, bool isunary) {
    if(op == "+") return isunary ? "ps" : "pl";
    if(op == "+=") return "pL";
    if(op == "++") return "pp";
    if(op == "-") return isunary ? "ng" : "mi";
    if(op == "-=") return "mI";
    if(op == "--") return "mm";
    if(op == "*") return isunary ? "de" : "ml";
    if(op == "*=") return "mL";
    if(op == "/") return "dv";
    if(op == "/=") return "dV";
    if(op == "%") return "rm";
    if(op == "%=") return "rM";
    if(op == "=") return "aS";
    // todo: handle spaces?
    if(op == "()") return "cl";
    if(op == "[]") return "ix";
    if(op == ">") return "gt";
    if(op == ">=") return "ge";
    if(op == "<") return "lt";
    if(op == "<=") return "le";
    if(op == "==") return "eq";
    if(op == "~") return "co";
    if(op == "->") return "pt";
    assert(0, format("unhandled operator '%s'",op));
}

string mangle_sigs(Fns...)(_CppFunction cf) if(Fns.length >= 1) {
    alias Fns[0] Fn;
    alias ParameterTypeTuple!Fn params;
    alias ParameterStorageClassTuple!Fn scs;
    alias variadicFunctionStyle!Fn variadic;
    string mangled;
    if(params.length == 0 && variadic == Variadic.no) {
        mangled ~= "v";
    }else{
        foreach(i, t; params) {
            mangled ~= mangle_type!(t)(cf,scs[i]);
        }
    }
    static assert(variadic != Variadic.typesafe);
    if(variadic == Variadic.c || variadic == Variadic.d) {
        // not really d style variadics, it's just for easier reading 
        mangled ~= "z";
    }
    return mangled;
}

string mangle_type(T, Context...)(_CppFunction cf, 
        uint sc = ParameterStorageClass.none,
        ) {
    string mangled = "";
    bool isRef = ((sc & ParameterStorageClass.ref_) != 0);
    if(is(T == ThisType)) {
        return "S_";
    }
    if(is(T == const) && !isPointer!T && ! isRef) {
        return mangle_type!(Unqual!T, Context)(cf,sc);
    }
    if(isRef) {
        mangled ~= "R";
        static if(is(T == const)) {
            mangled ~= "K";
            return mangled ~ (mangle_type!(Unqual!T, Context)(cf, 0));
        }
    }
    string m(string x) {
        return mangled ~ x;
    }
    static if(isPointer!T) {
        mangled ~= "P";
        static if(is(T == const)) {
            mangled ~= "K";
        }
        return m(mangle_type!(pointerTarget!T, Context)(cf));
    }else static if(isCppClass!(T)) {
        enum nom = __traits(identifier, T);
        if(nom == cf.classname && getNamespace!T == cf.namespace) {
            return m("S_");
        }else{
            foreach(i, ns_part; cf.ns_parts()) {
                mangled ~= mangle_name(ns_part);
            }
            mangled ~= mangle_name(nom);
            return mangled;
        }
    }else static if(is(T == ubyte)) {
        return m("h");
    }else static if(is(T == byte)) {
        return m("a");
    }else static if(is(T == ushort)) {
        return m("t");
    }else static if(is(T == short)) {
        return m("s");
    }else static if(is(T == uint)) {
        return m("j");
    }else static if(is(T == int)) {
        return m("i");
    }else static if(is(T == ulong)) {
        return m("m");
    }else static if(is(T == long)) {
        return m("l");
    }else static if(is(T == float)) {
        return m("f");
    }else static if(is(T == double)) {
        return m("d");
    }else static if(is(T == bool)) {
        return m("b");
    }else static if(is(T == char)) {
        // ugh! d's char isn't c's char!
        return m("c");
    }else assert(false, "unknown mangling for type " ~ T.stringof);
}



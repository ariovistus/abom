module abom.GCC;

import abom.common;
import std.traits;
import replace;
import abom.GCCMangler;

mixin template CppClass(string name, Opts...) if(isAnonymousClass!(Opts[$-1])) {
    import std.typetuple;
    import std.traits;
    import abom.common;
    import std.string;
    import abom.GCCMangler;
    import replace;
    import std.conv;
    import std.algorithm;
    import std.range;

    alias Opts[$-1] Clazz;
    alias CppOptions!(Opts[0 .. $-1]) Options;
    //alias Filter!(isCppOperator, DerivedFunctions!Clazz) ops1;
    alias fns1 = Filter!(templateAnd!(templateNot!isCppVirtual, 
                templateNot!isCppVirtual), DerivedFunctions!(Clazz));
    alias vfns = Filter!(isCppVirtual, DerivedFunctions!(Clazz));
    //pragma(msg, "Overriding Functions in " ~ name ~ ": ", vfns);
    alias fields = DerivedFields!Clazz;

    template cppFunc(alias FNC) {
        enum _CppFunction cppFunc = {
           namespace: Options.Namespace, 
           classname: name, 
           funcname: __traits(identifier, FNC),
           isoperator: isCppOperator!(FNC),
           operator: GetCppOperator!FNC
        };
    }
    enum Mangle(alias FNC) = mangle!(typeof(&FNC))(cppFunc!FNC);


    string BuildFunctionTemplate2(alias FNC, string sFNC, ARGS...)(int indent, string ftemplate, ARGS args) {
        import replace;
        import std.conv;
        import std.algorithm;
        enum cf = cppFunc!FNC;
        // todo: fix on res of dmd issue 12576
        enum funcname = cf.funcname;
        enum namespace = cf.namespace;
        enum operator = cf.operator;
        enum isoperator = cf.isoperator;
        alias ParameterTypeTuple!FNC params;
        alias ParameterIdentifierTuple!(FNC) PIT;
        enum bool isvoid = is(ReturnType!fnc == void);
        static if(PIT.length != 0) {
            enum ps = to!string(joiner([PIT], ","));
        }else{
            enum ps = "";
        }
        enum rt = BuildReplacedType2!(Options, name).Mappable!(ReturnType!FNC);
        enum string paramh = getparams2!(FNC, 
                format("ParameterDefaultValueTuple!(%s)", sFNC))(
                [staticMap!(BuildReplacedType2!(Options, name).Mappable, ParameterTypeTuple!FNC)]
                );
        string raw = replace2(ftemplate,
                    "$ReturnType", rt,
                    "$ClassName", name,
                    "$Params", paramh,
                    "$Name", funcname,
                    "$Args", ps,
                    "$sFunc", sFNC,
                    "$return ", (isvoid ? "" : "return "), 
                    args);
        return formatlines(raw, indent);
    }

    template funcMatch(alias FNC) {
        template funcMatch(alias FNC2) {
            enum funcMatch = __traits(identifier, FNC) == __traits(identifier, FNC2) &&
                is(ParameterTypeTuple!FNC == ParameterTypeTuple!FNC2);
        }
    }

    string formatlines(string s, int indent = 1) {
        auto lines = splitter(s.outdent().stripRight(), "\n");
        string indent_s = to!string(joiner(repeat("    ", indent)));
        return indent_s ~ to!string(joiner(lines, "\n" ~ indent_s));
    }

    string BuildVirtualTable() {
        import replace;
        alias superclass_ixs = Options.Inherits.withVirtualTable;
        string bdy = "";
        string vtable = formatlines(q{
                size_t offset0;
                void* something0;
        },1);
        size_t class_index = 0;
        bool[size_t] written_funcs;

        static if(superclass_ixs.length != 0) {
            alias superclass0 = Options.Inherits.Supers[superclass_ixs[0]];
            enum superclass_name = fullyQualifiedName!superclass0;
            alias super_vfns = Options.Inherits.VirtualFunctions!(superclass_ixs[0]);
            enum super_vfns_s = "Options.Inherits.VirtualFunctions!(%s)[%s]";
            foreach(i, fnc; super_vfns) {
                enum f_override = staticFind!(funcMatch!(fnc), vfns);
                enum string thistype = f_override.found ? name : superclass_name;

                vtable ~= BuildFunctionTemplate2!(fnc, format(super_vfns_s, superclass_ixs[0], i))(1, q{
                    extern(C) $ReturnType function($ThisType* _this, $Params) $Name;
                }, "$ThisType", thistype);

                if(f_override.found) {
                    written_funcs[f_override.index] = true;
                    bdy ~= BuildFunctionTemplate2!(fnc, format(super_vfns_s, superclass_ixs[0],i))(1, q{
                        @CppVirtual
                        $ReturnType $Name($Params) {
                            $return _vtable.$Name(&this, $Args);
                        }
                    });
                }else{

                    bdy ~= BuildFunctionTemplate2!(fnc, format(super_vfns_s, superclass_ixs[0],i))(1, q{
                        @CppVirtual
                        $ReturnType $Name($Params) {
                            $return _vtable.$Name(this.Super!($Supers0*)(), $Args);
                        }
                    }, "$Supers0", superclass_name);
                }
            }
        }
        if(vfns.length != 0) {
            foreach(i, fnc; vfns) {
                if ( i in written_funcs) continue;

                vtable ~= BuildFunctionTemplate2!(fnc, format("vfns[%s]",i))(1, q{
                    extern(C) $ReturnType function($ClassName* _this, $Params) $Name;
                });

                bdy ~= BuildFunctionTemplate2!(fnc, format("vfns[%s]",i))(1, q{
                    @CppVirtual
                    $ReturnType $Name($Params) {

                        $return _vtable.$Name(&this, $Args);
                    }
                });
            }

        }
        static if(superclass_ixs.length > 1) {
            foreach(_, j; superclass_ixs[1..$]) {
                alias superclassj = Options.Inherits.Supers[j];
                alias super_vfnsr = Options.Inherits.VirtualFunctions!(j);
                enum string super_vfns_sr = "Options.Inherits.VirtualFunctions!(%s)[%s]";
                enum string thistype = fullyQualifiedName!superclassj; //replace2("Options.Inherits.Supers[$j]", "$j", j);
                vtable ~= formatlines(replace2("size_t offset$j;\n", "$j", j),2);
                vtable ~= formatlines(replace2("void* something$j;\n", "$j", j),2);
                foreach(i, fnc; super_vfnsr) {
                    enum f_override = staticFind!(funcMatch!(fnc), vfns);
                    if(f_override.found) {
                        vtable ~= BuildFunctionTemplate2!(fnc, format(super_vfns_sr, j, i))(1, q{
                            extern(C) $ReturnType function($ThisType* _this, $Params) $Name_thunk$j;
                        }, 
                        "$ThisType",thistype, "$j", j);
                    }else{
                        bdy ~= BuildFunctionTemplate2!(fnc, format(super_vfns_s, j,i))(1, q{
                            @CppVirtual
                            $ReturnType $Name($Params) {
                                $return _vtable.$Name(this.Super!($ThisType*)(), $Args);
                            }
                        },
                        "$ThisType",thistype);
                        vtable ~= BuildFunctionTemplate2!(fnc, format(super_vfns_sr, j, i))(1, q{
                            extern(C) $ReturnType function($ThisType* _this, $Params) $Name;
                        }, 
                        "$ThisType",thistype);
                    }
                }
            }
        }
        if (superclass_ixs.length + vfns.length != 0) {
            bdy ~= formatlines(replace2(q{
                struct _VTable {
                    $VTablePtrs
                }
            }.outdent(), "$VTablePtrs", vtable), 1);
        }
        return bdy;
    }

    string BuildFields() {
        string bdy = "";
        if(Options.Inherits.any) {
            foreach(i, SuperType; Options.Inherits.Supers) {
                bdy ~= formatlines(replace2(q{
                    $Type super$i;
                    $Type* Super(T)() if(is(T == $Type*)) {
                        return &super$i;
                    }
                }.outdent(), 
                "$Type", fullyQualifiedName!SuperType,
                "$i", i,
                ), 1);
            }
            bdy ~= formatlines(q{
                @property _VTable* _vtable() {
                    return cast(_VTable*) (super0._vtable);
                }});
        }else if (vfns.length != 0) {
            bdy ~= formatlines(q{
                void* _vtable_;
                });
            bdy ~= formatlines(q{
                @property _VTable* _vtable() {
                    return cast(_VTable*) (_vtable_-size_t.sizeof-(void*).sizeof);
                }});
        }
        foreach(i, string field_name; fields) {
            bdy ~= formatlines(replace2(q{
                    $Type $Name;
            }.outdent(), 
                "$Type", fullyQualifiedName!(typeof(mixin("Clazz.init." ~ field_name))),
                "$Name", field_name,
                ), 1);
        }
        return bdy;
    }

    CppClassMixins BuildCppClassMixins() {
        import replace;
        import std.algorithm;
        import std.conv;
        import std.string;
        import abom.common;
        import abom.GCCMangler;
        import std.traits;
        import std.array;

        bool inherits = Options.Inherits.Supers.length != 0;
        bool virtualInherits = anySatisfy!(hasVirtualTable, Options.Inherits.Supers);
        static if(Options.hasTemplate) {
            enum string classname = name ~ format("(%s)", joiner(Options.TemplateParams, ", "));
        }else {
            enum string classname = name;
        }
        string bdy = "";
        string externs = "";
        if(virtualInherits || vfns.length != 0) {
            bdy ~= BuildVirtualTable();
        }
        bdy ~= BuildFields();
        alias DerivedFields!(Clazz) fields;
        foreach(i, fnc; fns1) {
            enum sfnc = format("fns1[%s]", i);
            enum mangled = Mangle!fnc;
            static if(__traits(isStaticFunction, fnc)) {
                externs ~= BuildFunctionTemplate2!(fnc, sfnc)(1, q{
                    pragma(mangle, "$MangledName") extern(C) static $ReturnType $MangledName($Params);
                }, "$MangledName", mangled);

                bdy ~= BuildFunctionTemplate2!(fnc, sfnc)(1, q{
                    static $ReturnType $Name($Params) {
                        $return $MangledName($Args);
                    }
                }, "$MangledName", mangled);
            }else{
                externs ~= BuildFunctionTemplate2!(fnc, sfnc)(1, q{
                    pragma(mangle, "$MangledName") extern(C) static $ReturnType $MangledName($ClassName* _this, $Params);
                }, "$MangledName", mangled);

                bdy ~= BuildFunctionTemplate2!(fnc, sfnc)(1, q{
                    $ReturnType $Name($Params) {
                        $return $MangledName(&this, $Args);
                    }
                }, "$MangledName", mangled);

            }
        }
        string mix = formatlines(replace2(q{
                @ACppClass 
                @CppNamespace("$Namespace")
                struct $CppClassName {
                    $Body
                }
                }.outdent(),
                "$Namespace", Options.Namespace,
                "$CppClassName", classname, 
                "$Body",externs ~ bdy), 0);
        return CppClassMixins(mix);
    }
    enum Mixins = BuildCppClassMixins();
    pragma(msg, (Mixins.def));
    mixin(Mixins.def);
}

struct CppClassMixins{
    string def;
    string externs;
}

template CppFunction(string name, FnType, Opts...) 
if(is(FnType == function) || isFunctionPointer!FnType) {
    import std.typetuple;
    import std.traits;

    alias CppOptions!(Opts) Options;
    enum _CppFunction cf = {
        namespace: Options.Namespace, 
        classname: null, 
        funcname: name
    };
    static if(is(FnType == function)) {
        enum sFnType = typeof(&FnType.init).stringof;
    }else{
        enum sFnType = FnType.stringof;
    }
    enum sPs = getparams2!(FnType, 
            "ParameterDefaultValueTuple!(" ~ sFnType ~ ")")(
                [staticMap!(BuildReplacedType2!(Options, null, false).Mappable, ParameterTypeTuple!FnType)]
            );
    enum mangledName = mangle!FnType(cf);
    enum CppFunction = replace2(q{
            extern(C) $ReturnType $Fonc($Params);
            alias $Finc = $Fonc;
    },
        "$Fonc",  mangledName,
        "$ReturnType", fullyQualifiedName!(ReturnType!FnType),
        "$Params", sPs,
        "$Finc", name);
    //pragma(msg, CppFunction);
}

template BuildReplacedType2(Options, string name, bool replaceThisType = true) {
    template Mappable(T) {
        string inner() {
            string result = fullyQualifiedName!T;
            if(replaceThisType) {
                result = replace2(result, "abom.common.ThisType", name);
            }
            static if(Options.hasTemplate) {
                foreach(param; Options.TemplateParams) {
                    string spec = format("abom.common.TArg!%s", 
                            escapeStringLiteral(param));
                    result = replace2(result, spec , param);
                }
            }
            return result;
        }
        enum Mappable = inner();
    }
}

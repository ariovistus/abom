module abom.params;

import std.algorithm;
import std.typetuple;

/*
template IsString(thing...) {
    enum IsString = is(typeof(thing[0]) == string);
}

struct CppNamespace(name...) {
    static assert(Filter!(IsString, name).length == name.length);
    alias name NameParts;
}
template IsCppNamespace(T...) {
    enum bool IsCppNamespace = T[0].stringof.startsWith("CppNamespace!");
}

struct CppClassName(name...){
    static assert(1 == name.length);
    static assert(Filter!(IsString, name).length == name.length);
    enum string Name = name[0];
}
template IsCppClassName(T...) {
    enum bool IsCppClassName = T[0].stringof.startsWith("CppClassName!");
}

struct CppFieldName(name...){
    static assert(1 == name.length);
    static assert(Filter!(IsString, name).length == name.length);
    enum string Name = name[0];
}
template IsCppFieldName(T...) {
    enum bool IsCppFieldName = T[0].stringof.startsWith("CppFieldName!");
}

template Params(P...) {
    alias Filter!(IsCppNamespace, P) Namespace;
    alias Filter!(IsCppFieldName, P) FieldName;
    alias Filter!(IsCppClassName, P) ClassName;
    alias Filter!(templateAnd!(
                templateNot!IsCppNamespace,
                templateNot!IsCppClassName, 
                templateNot!IsCppFieldName), 
            P) Rest;
}
*/

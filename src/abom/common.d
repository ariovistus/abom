module abom.common;

import std.algorithm;
import std.typetuple;
import std.traits;
import std.string;
import std.range;
import replace;

struct CppOptions(Opts...) {
    alias Options = Opts;
    static if(anySatisfy!(isCppInherits, Opts)) {
        alias Inherits = Filter!(isCppInherits, Opts)[0];
    }else{
        alias Inherits = CppInherits!();
    }
    static assert(allSatisfy!(isCppClass, Inherits.Supers));

    static if(anySatisfy!(isCppNamespaceAttr, Opts)) {
        enum Namespace = Filter!(isCppNamespaceAttr, Opts)[0].Namespace;
    }else{
        enum Namespace = "";
    }
    enum hasTemplate = anySatisfy!(isCppTemplate, Opts);
    static if(hasTemplate) {
        enum TemplateParams = Filter!(isCppTemplate, Opts)[0].Params;
    }else{
        enum string[] TemplateParams = [];
    }
}

struct CppInherits(T...) {
    alias Supers = T;

    alias VirtualFunctions(size_t i) = Filter!(isCppVirtual, DerivedFunctions!(Supers[i]));

    /// returns tuple of indeces into Supers pointing to supers that have virtual tables
    template _withVirtualTable(size_t i) {
        static if(i == Supers.length) {
            alias _withVirtualTable = TypeTuple!();
        }else static if(hasVirtualTable!(Supers[i])) {
            alias _withVirtualTable = TypeTuple!(i, _withVirtualTable!(i+1));
        }else {
            alias _withVirtualTable = _withVirtualTable!(i+1);
        }
    }

    alias withVirtualTable = _withVirtualTable!(0);
    enum any = Supers.length != 0;
}

enum isCppInherits(T...) = T[0].stringof.startsWith("CppInherits!");

struct CppVirtual {}

enum isCppVirtualAttr(T...) = is(T[0] == CppVirtual);
enum isCppVirtual(Fn...) = anySatisfy!(isCppVirtualAttr,__traits(getAttributes, Fn[0]));

enum hasVirtualTable(T) = __traits(compiles, {alias x = T._VTable;});


struct CppOperator{
    string Operator;
}
enum isCppOperatorAttr(T...) = is(typeof(T[0]) == CppOperator);
enum isCppOperator(Fn...) = anySatisfy!(isCppOperatorAttr,__traits(getAttributes, Fn[0]));

template GetCppOperator(alias fn) {
    static if(isCppOperator!fn) {
        enum GetCppOperator = Filter!(isCppOperatorAttr,__traits(getAttributes, fn))[0].Operator;
    }else{
        enum string GetCppOperator = null;
    }
}

unittest {
    class X {
        @CppOperator("+") void a(int i) {}
    }

    static assert(isCppOperator!(__traits(getMember, X, "a")));
}

struct CppNamespace {
    string Namespace;
}

struct ThisType{}

struct TArg(string argName) { }

template getNamespace(T) {
    alias Ns = Filter!(isCppNamespaceAttr,__traits(getAttributes, T));
    //enum _CppFunction cf = {namespace: Ns[0].Namespace};
    enum getNamespace = Ns[0].Namespace;
}

enum isCppNamespaceAttr(T...) = is(typeof(T[0]) == CppNamespace);

struct CppInherit(T) {
    alias Type = T;
}

enum isStringValue(T...) = T.length == 1 && is(typeof(T) == string);

struct CppTemplateParams {
    string[] Params;
    this(string[] ps...) {
        Params = ps;
    }
}
enum isCppTemplate(Ts...) = is(typeof(Ts[0]) == CppTemplateParams);


struct _CppFunction {

    string namespace;
    string classname;
    string funcname;
    string operator;
    bool isoperator = false;

    string[] ns_parts () {
        import std.algorithm;
        import std.array;
        return array(splitter(namespace, "::"));
    }
}
struct ACppClass {}
enum isCppClassAttr(T...) = is(T[0] == ACppClass);

template isCppClass(T) {
    static if(is(T == struct)) {
        enum isCppClass = anySatisfy!(isCppClassAttr,__traits(getAttributes,T));
    }else{
        enum isCppClass = false;
    }
}
enum isAnonymousClass(A) = A.stringof.startsWith("__anonclass");

template DerivedFunctions(A) {
    template Sym(string member) {
        alias TypeTuple!(__traits(getOverloads, A, member)) Sym;
    }
    template isFunction(T...) {
        enum isFunction = is(typeof(T[0]) == function);
    }
    alias DerivedFunctions = Filter!(isFunction,
            staticMap!(Sym, __traits(derivedMembers, A)));
}

template DerivedFields(A) {
    template isFunction(T...) {
        enum isFunction = is(typeof(__traits(getMember,A,T[0])) == function);
    }

    alias DerivedFields = Filter!(templateNot!isFunction,
            __traits(derivedMembers, A));
}
template DerivedFieldTypes(A) {
    alias TypeofField(string f) = typeof(mixin("A."~f));
    alias DerivedFieldTypes = staticMap!(TypeofField, DerivedFields!(A));
}



// from pyd
/**
  Get the parameters of function as a string.

  pt_alias refers to an alias of ParameterTypeTuple!fn
  visible to wherever you want to mix in the results.
  pd_alias refers to an alias of ParameterDefaultValueTuple!fn
  visible to wherever you want to mix in the results.
Example:
---
void foo(int i, int j=2) {
}

static assert(getparams!(foo,"P","Pd") == "P[0] i, P[1] j = Pd[1]");
---
  */
template getparams(Args...) if(Args.length == 3 && isCallable!(Args[0]) 
        && is(typeof(Args[1]) == string) && is(typeof(Args[2]) == string)) {
    alias fn = Args[0];
    enum pt_alias =  Args[1];
    enum pd_alias = Args[2];
    alias ParameterIdentifierTuple!fn Pi;
    alias ParameterDefaultValueTuple!fn Pd;
    alias ParameterStorageClassTuple!fn Psc;
    enum var = variadicFunctionStyle!fn;

    string inner() {
        static if(var == Variadic.c || var == Variadic.d) {
            return "...";
        }else{
            string ret = "";
            foreach(size_t i, id; Pi) {
                static if((Psc[i] & ParameterStorageClass.ref_) != 0) {
                    ret ~= "ref ";
                }
                ret ~= format("%s[%s] %s", pt_alias, i, id);
                static if(!is(Pd[i] == void)) {
                    ret ~= format(" = %s[%s]", pd_alias, i);
                }
                static if(i != Pi.length-1) {
                    ret ~= ", ";
                }
            }
            static if(var == Variadic.typesafe) {
                ret ~= "...";
            } 
            return ret;
        }
    }

    enum getparams = inner();

}

string toValueString(T...)(string pd_alias, size_t i) if (T.length == 1) {
    import std.conv;
    static if(__traits(compiles, mixin(to!string(T[0])))) {
        static if(mixin(to!string(T[0])) == T[0]) {
            return to!string(T[0]);
        }
    }
    return format("/* %s */ %s[%s]", T[0], pd_alias, i);
}

string getparams2(Args...)(string[] pt_alias) 
if(Args.length == 2 && isCallable!(Args[0]) && is(typeof(Args[1]) == string)) {

    alias fn = Args[0];
    enum pd_alias = Args[1];
    alias ParameterIdentifierTuple!fn Pi;
    alias ParameterDefaultValueTuple!fn Pd;
    alias ParameterStorageClassTuple!fn Psc;
    enum var = variadicFunctionStyle!fn;

    static if(var == Variadic.c || var == Variadic.d) {
        return "...";
    }else{
        string ret = "";
        foreach(size_t i, id; Pi) {
            static if((Psc[i] & ParameterStorageClass.ref_) != 0) {
                ret ~= "ref ";
            }
            ret ~= format("%s %s", pt_alias[i], id);
            static if(!is(Pd[i] == void)) {
                ret ~= format(" = %s", toValueString!(Pd[i])(pd_alias, i));
            }
            static if(i != Pi.length-1) {
                ret ~= ", ";
            }
        }
        static if(var == Variadic.typesafe) {
            ret ~= "...";
        } 
        return ret;
    }
}


struct zTuple(T...) {
    alias Stuff = T;
}

// stolen from std.traits
// todo: make request to have parameter types and parameter storage classes
// parameters of this template in std.traits
string parametersTypeString1(_parameters, _parameterStC, Variadic variadic)() @property
{
    import std.array, std.algorithm, std.range;

    alias parameters = _parameters.Stuff;
    alias parameterStC = _parameterStC.Stuff;

    static if (variadic == Variadic.no)
        enum variadicStr = "";
    else static if (variadic == Variadic.c)
        enum variadicStr = ", ...";
    else static if (variadic == Variadic.d)
        enum variadicStr = parameters.length ? ", ..." : "...";
    else static if (variadic == Variadic.typesafe)
        enum variadicStr = " ...";
    else
        static assert(0, "New variadic style has been added, please update fullyQualifiedName implementation");

    static if (parameters.length)
    {
        string result = join(
                map!(a => format("%s%s", a[0], a[1]))(
                    zip([staticMap!(storageClassesString, parameterStC)],
                        [staticMap!(fullyQualifiedName, parameters)])
                    ),
                ", "
                );

        return result ~= variadicStr;
    }
    else
        return variadicStr;
}

    string storageClassesString(uint psc)() @property
    {
        alias ParameterStorageClass PSC;

        return format("%s%s%s%s",
            psc & PSC.scope_ ? "scope " : "",
            psc & PSC.out_ ? "out " : "",
            psc & PSC.ref_ ? "ref " : "",
            psc & PSC.lazy_ ? "lazy " : ""
        );
    }
    string linkageString(T)() @property
    {
        enum linkage = functionLinkage!T;

        if (linkage != "D")
            return format("extern(%s) ", linkage);
        else
            return "";
    }

    string functionAttributeString(T)(string mode = "none") @property
    {
        alias FunctionAttribute FA;
        enum attrs = functionAttributes!T;

        static if (attrs == FA.none)
            return "";
        else
            return format("%s%s%s%s%s%s",
                 (attrs & FA.pure_) && mode != "pre" ? " pure" : "",
                 (attrs & FA.nothrow_) && mode != "pre" ? " nothrow" : "",
                 (attrs & FA.ref_) && mode == "pre" ? " ref" : "",
                 (attrs & FA.property) && mode != "pre" ? " @property" : "",
                 (attrs & FA.trusted) && mode !="pre" ? " @trusted" : "",
                 (attrs & FA.safe) && mode != "pre" ? " @safe" : ""
            );
    }

template ReplaceType(Orig, OldT, NewT) {
    template Mappable(Ts...) {
        static if(is(ReplaceType!(Ts[0], OldT, NewT))) {
            alias Mappable = ReplaceType!(Ts[0], OldT, NewT);
        }else{
            alias Mappable = Alias!(Ts[0]);
        }
    }
    static if(is(Orig == OldT)) {
        alias ReplaceType = NewT;
    }else static if(is(Orig : Template!Args, alias Template, Args...)) {
        alias NewArgs = staticMap!(Mappable, Args);
        alias ReplaceType = Template!NewArgs;
    }else static if(isFunctionPointer!Orig || isDelegate!Orig) {
        alias RT = ReplaceType!(ReturnType!Orig, OldT, NewT);
        alias SC = ParameterStorageClassTuple!Orig;
        alias PS = staticMap!(Mappable, ParameterTypeTuple!Orig);
        enum variadic = variadicFunctionStyle!Orig;

        enum params0 = parametersTypeString1!(zTuple!PS, zTuple!SC, variadic);
        // grr can't have these in param sig
        enum params = replace.Replace!(params0, 
                "extern(C)", "",
                "extern(Windows)", "",
                "extern(Pascal)", "",
                "extern(C++)", "");

        enum x = (replace.Replace!(q{
            alias $extern $FattrsPre RT $Function($Params) $FattrsPost ReplaceType;
            }, 
            "$extern", linkageString!Orig,
            "$Function", isDelegate!Orig ? "delegate" : "function",
            "$FattrsPre", functionAttributeString!Orig("pre"),
            "$FattrsPost", functionAttributeString!Orig("post"),
            "$Params", params));
        mixin(x);
    }else static if(isPointer!Orig) {
        alias ReplaceType = ReplaceType!(pointerTarget!Orig, OldT, NewT)*;
    } else static if (isDynamicArray!Orig) {
        import std.range;
        alias ReplaceType = ReplaceType!(ElementType!Orig, OldT, NewT)[];
    } else static if (isStaticArray!Orig) {
        import std.range;
        alias ReplaceType = ReplaceType!(ElementType!Orig, OldT, NewT)[Orig.length];
    } else{
        alias ReplaceType = Orig;
    }
}

template ReplaceTypeT(OldNewPairs...) {
    template ReplaceTypeT(Orig) {
        template ReduceZip2(RT, _OldNewPairs...) {
            static if(_OldNewPairs.length == 0) {
                alias ReduceZip2 = RT;
            }else{
                alias ReduceZip2 = ReduceZip2!(ReplaceType!(RT, 
                            _OldNewPairs[0], 
                            _OldNewPairs[1]), _OldNewPairs[2..$]);
            }
        }
        alias ReplaceTypeT = ReduceZip2!(Orig, OldNewPairs);
    }
}

unittest {
    static assert(is(ReplaceType!(int, int, string) == string));
    static assert(is(ReplaceType!(double, int, string) == double));
    static assert(is(ReplaceType!(int[], int, string) == string[]));
    static assert(is(ReplaceType!(int[1], int, string) == string[1]));
    static assert(is(ReplaceType!(int function(int), int, string) == 
                string function(string)));
    alias ref int function(int) Dgi1;
    alias ref string function(string) Dgs1;
    static assert(is(ReplaceType!(Dgi1, int, string) == Dgs1));
    static assert(is(ReplaceType!(int delegate(int), int, string) == 
                string delegate(string)));
    alias extern(C) int delegate(ref int function(ref int)) pure @trusted Dg1;
    alias extern(C) string delegate(ref string function(ref string)) pure @trusted Dg2;
    alias ReplaceType!(Dg1,int,string) Dg22;
    class testbunk(string a) { }
    static assert(is(ReplaceType!(Dg1,int,string) == Dg2));
    static assert(is(ReplaceType!(Tuple!int,int,string) == Tuple!string));
    static assert(is(ReplaceType!(testbunk!"abc",int,string) == testbunk!"abc"));

    static assert(is(staticMap!(ReplaceTypeT!(int,string), int, int[], float, int*) == 
                TypeTuple!(string, string[], float, string*)));
}

string escapeStringLiteral(string str) {
    return format("%(%s%)", [str]);
}

unittest {
    import std.stdio;
    writeln("esc: ", escapeStringLiteral("abc\ndef"));
    assert(escapeStringLiteral("abc\ndef") == `"abc\ndef"`);
}

struct FindResults {
    size_t index;
    bool found;
}
template staticFind(alias Pred, T...) {
    template inner(size_t index) {
        static if(T.length == index) {
            enum inner = FindResults(size_t.max, false);
        }else static if(Pred!(T[index])) {
            enum inner = FindResults(index, true);
        }else {
            enum inner = inner!(index+1);
        }
    }

    enum staticFind = inner!0;
}

string lineNumbers(string code) {
    import std.array;
    auto lines = array(splitter(code, "\n"));
    auto nums = iota(0, lines.length);
    return reduce!((s, t) => format("%s\n%s: %s", s, t[0], t[1]))("", zip(nums, lines));
}

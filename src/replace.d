module replace;
import std.algorithm;
import std.range;
import std.array;
import std.conv;
import std.typetuple;

/**
Performs compile time string replacements on $(D base)

Parameters:

T = replacement specs, alternating between string to be replaced and $(D toStringNow)-able object to replace with.

Example:
---
import std.metastrings;
import std.stdio;

void main()
{
  string s = Replace!(q{$ret func(T)(T t){ return new $ret(t+$i); }},
    "$ret", "C",
    "$i", 5000);
  writeln(s); // "C func(T)(T t){ return new C(t+5000); }"
}
---
If there is ambiguity between two substrings to replace, the longer one is preferred:
---
enum s = Replace!("boy eats boysenberry", "boy", "girl", "boysenberry", "plum");
writeln(s) // "girl eats plum"
---
 */
template Replace(string base, T...) 
{
    import std.algorithm;
    static assert(T.length % 2 == 0);
    template NextAt(string base, string longest_spec, 
            size_t _at0, size_t _ti0, T...)
    {
        static assert(T.length % 2 == 0);
        static if(T.length == 0)
        {
            static if(_at0 == -1) 
            {
                enum size_t at = base.length;
                enum size_t ti = -1;
            }
            else
            {
                enum at = _at0;
                enum ti = _ti0;
            }
        }
        else
        {
            enum size_t _at1 = countUntil(base, T[$-2]);
            static if(_at1 < _at0 || 
                    _at1 == _at0 && T[$-2].length > longest_spec.length)
            {
                alias NextAt!(base, T[$-2], _at1, T.length-2,T[0 .. $-2]) N2;
            }
            else 
            {
                alias NextAt!(base,longest_spec,_at0,_ti0,T[0 .. $-2]) N2;
            }
            enum at = N2.at;
            enum ti = N2.ti;
        }
    }


    alias NextAt!(base,"",-1,-1,T) N;
    static if(N.ti == -1)
        enum Replace = base;
    else
        enum Replace = base[0 .. N.at] ~ to!string(T[N.ti+1]) ~ 
            Replace!(base[N.at + T[N.ti].length .. $], T);
}

template Rest(T...) {
    static if(T.length == 0) {
        alias Rest = T;
    }else{
        alias Rest = T[1 .. $];
    }
}

unittest {
    static assert(is(Rest!() == TypeTuple!()));
    static assert(is(Rest!(int, double, float) == TypeTuple!(double, float)));
}

alias LooseSlice(size_t start, size_t end, Tx...) = Tx[min(start, Tx.length) .. min(end, Tx.length)];

unittest {
    static assert(is(LooseSlice!(0, 1, TypeTuple!(int, int, int)) == TypeTuple!(int)));
    static assert(is(LooseSlice!(0, 10, TypeTuple!(int, int, int)) == TypeTuple!(int,int,int)));
    static assert(is(LooseSlice!(10, 10, TypeTuple!(int, int, int)) == TypeTuple!()));
    static assert(is(LooseSlice!(1, 10, TypeTuple!(int, uint, long)) == TypeTuple!(uint,long)));
}

/// from clojure's partition-all, except not lazy and flattened, because these are type tuples, *(&^&!
template FlatIndexPartition(size_t n, size_t step, T...) {
    static if(T.length <= step) {
        alias FlatIndexPartition = LooseSlice!(0, n, T);
    }else {
        alias FlatIndexPartition = TypeTuple!(LooseSlice!(0, n, T), FlatIndexPartition!(n, step, LooseSlice!(step, T.length, T)));
    }
}

unittest {
    alias g = FlatIndexPartition!(1,2,TypeTuple!(int,double,float));
    static assert(is(g == TypeTuple!(int,float)));
}

alias EvenFilter(T...) = FlatIndexPartition!(1, 2, T);
alias OddFilter(T...) = FlatIndexPartition!(1, 2, Rest!T);

unittest {
    static assert(is(OddFilter!(TypeTuple!()) == TypeTuple!()));
    static assert(is(OddFilter!(TypeTuple!(int)) == TypeTuple!()));
    static assert(is(OddFilter!(TypeTuple!(int, string, double, float)) == TypeTuple!(string, float)));
}

enum isString(T...) = is(T[0] == string);
enum isReplaceSpec(T...) = T.length % 2 == 0 && allSatisfy!(isString, EvenFilter!T);

template strings(V...) {
    static if(V.length == 0) {
        alias strings = TypeTuple!();
    }else {
        alias strings = TypeTuple!(to!string(V[0]), V[1..$]);
    }
}

string replace2(T...)(string format, T args) if (isReplaceSpec!T) {
    auto specs = [EvenFilter!args];
    bool less(size_t a, size_t b) { 
        return specs[a].length < specs[b].length;
    }
    size_t[] sorted_indeces = array(sort!less(array(iota(0, specs.length))));
    if(specs.length > 0) assert(specs[sorted_indeces[0]] != "", "replacing empty strings makes no sense!");
    alias replacements_tuple = OddFilter!args;
    string[] replacements = new string[](replacements_tuple.length); 
    foreach(i, _; replacements_tuple) {
        replacements[i] = to!string(replacements_tuple[i]);
    }
    string[] chunks;

    string current_format = format;
    while(true) {
        size_t current_index = current_format.length;
        string current_spec = "";
        size_t current_spec_index = specs.length;

        foreach(unsorted_i, sorted_i; sorted_indeces) {
            string spec = specs[sorted_i];
            auto index = countUntil(current_format, spec);
            if (index == -1) continue;
            if (index < current_index || 
                    index == current_index && spec.length > current_spec.length) {
                current_index = index;
                current_spec = spec;
                current_spec_index = sorted_i;
            }
        }
        if (current_spec != "") {
            chunks ~= current_format[0 .. current_index];
            chunks ~= to!string(replacements[current_spec_index]);
            current_format = current_format[current_index + current_spec.length .. $];
        }else {
            break;
        }
    }
    chunks ~= current_format;
    return to!string(joiner(chunks,""));
}

unittest {
    assert(replace2("abc $c def", "$c", 11) == "abc 11 def");
    assert(replace2("abc $c def", "$c", "ee") == "abc ee def");
    assert(replace2("abc $caramba def", "$c", "ee", "$caramba", "ff") == "abc ff def");
    assert(replace2("abc $caramba def", "$caramba", "ee", "$c", "ff") == "abc ee def");
    assert(replace2("abc $c def", "$c", "$caramba", "$caramba", "ff") == "abc $caramba def");
    import std.exception;
    import core.exception;
    assert(collectException!AssertError(replace2("abc $c def", "", "caramba!")));
}

unittest {
    static assert(replace2("abc $c def", "$c", 11) == "abc 11 def");
    static assert(replace2("abc $c def", "$c", "ee") == "abc ee def");
    static assert(replace2("abc $caramba def", "$c", "ee", "$caramba", "ff") == "abc ff def");
    static assert(replace2("abc $c def", "$c", "$caramba", "$caramba", "ff") == "abc $caramba def");
    static assert(!__traits(compiles,collectException!AssertError(replace2("abc $c def", "", "caramba!"))));
}
